package WIP_Raquel;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Container;
import java.awt.Toolkit;
import javax.swing.border.MatteBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

public class cine_window extends JFrame {


	private JPanel panel_cine;
	protected Container buttonGroup;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cine_window frame = new cine_window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public cine_window() {
		
		//Ventana
		setIconImage(Toolkit.getDefaultToolkit().getImage(cine_window.class.getResource("/img/imagenes/icon app.png")));
		setTitle("Cine");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 878, 674);
		
		//Panel
		panel_cine = new JPanel();
		panel_cine.setBackground(Color.WHITE);
		panel_cine.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel_cine);
		panel_cine.setLayout(null);
	
			
		//Botones
		JButton boton1 = new JButton("Cartelera");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cine_SQL cine1 = new cine_SQL();
				try {
					
					cine1.SQLConnection("proyecto", "root", "");
					
					cine_window_cartelera w1 = new cine_window_cartelera();
					w1.setVisible(true);
										
					cine1.closeConnection();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		boton1.setBackground(new Color(255, 69, 0));
		boton1.setForeground(new Color(255, 255, 255));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton1.setBounds(10, 15, 159, 43);
		panel_cine.add(boton1);
		//
		
		
		JButton boton2 = new JButton("Estrenos");
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cine_window_estrenos w1 = new cine_window_estrenos();
				w1.setVisible(true);
			}
		});
		boton2.setBackground(new Color(255, 69, 0));
		boton2.setForeground(new Color(255, 255, 255));
		boton2.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton2.setBounds(179, 15, 146, 43);
		panel_cine.add(boton2);
		
		JButton boton3 = new JButton("Tr\u00E1ilers");
		boton3.setBackground(new Color(255, 69, 0));
		boton3.setForeground(new Color(255, 255, 255));
		boton3.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton3.setBounds(716, 66, 124, 43);
		panel_cine.add(boton3);
		
		JButton boton4 = new JButton("Pr\u00F3ximos estrenos");
		boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				cine_w_proxestrenos w1 = new cine_w_proxestrenos();
				w1.setVisible(true);
				
				
			}
		});
		boton4.setBackground(new Color(255, 69, 0));
		boton4.setForeground(new Color(255, 255, 255));
		boton4.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton4.setBounds(335, 15, 170, 43);
		panel_cine.add(boton4);
		
		JButton boton5 = new JButton("Pr\u00F3ximamente en cine");
		boton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cine_w_prox w1 = new cine_w_prox();
				w1.setVisible(true);
				
			}
		});
		boton5.setBackground(new Color(255, 69, 0));
		boton5.setForeground(new Color(255, 255, 255));
		boton5.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton5.setBounds(515, 15, 176, 42);
		panel_cine.add(boton5);
		
		JButton boton6 = new JButton("OFERTAS");
		boton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		boton6.setBackground(new Color(255, 69, 0));
		boton6.setForeground(new Color(255, 255, 255));
		boton6.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton6.setBounds(716, 15, 124, 43);
		panel_cine.add(boton6);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(10, 622, 146, 14);
		panel_cine.add(progressBar);
		
		

		
		//Foto fondo (etiqueta)
		JLabel foto1 = new JLabel("");
		foto1.setIcon(new ImageIcon(cine_window.class.getResource("/img/imagenes/cine.PNG")));
		foto1.setBounds(24, 69, 784, 551);
		panel_cine.add(foto1);
		
	
		
	}
}
