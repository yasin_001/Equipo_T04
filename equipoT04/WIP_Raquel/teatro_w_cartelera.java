package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class teatro_w_cartelera extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					teatro_w_cartelera frame = new teatro_w_cartelera();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public teatro_w_cartelera() {
		setTitle("Cartelera Teatro");
		setIconImage(Toolkit.getDefaultToolkit().getImage(teatro_w_cartelera.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 387, 276);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEstaSemanaTenemos = new JLabel("Esta semana tenemos...");
		lblEstaSemanaTenemos.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		lblEstaSemanaTenemos.setForeground(new Color(128, 0, 128));
		lblEstaSemanaTenemos.setBounds(35, 11, 258, 26);
		contentPane.add(lblEstaSemanaTenemos);
		
		JRadioButton rboton1 = new JRadioButton("Funci\u00F3n infantil", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setBounds(37, 60, 147, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("El musical de disney", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(37, 102, 147, 23);
		contentPane.add(rboton2);
		
		JRadioButton rboton3 = new JRadioButton("Ballet 2016", false);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton3.setBounds(37, 144, 147, 23);
		contentPane.add(rboton3);
		
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		grupoBR.add(rboton3);
		
		
		
		JButton boton = new JButton("COMPRAR ENTRADAS");
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		boton.setForeground(new Color(139, 0, 0));
		boton.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton.setBounds(133, 193, 201, 34);
		contentPane.add(boton);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(teatro_w_cartelera.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 381, 238);
		contentPane.add(lblNewLabel);
	}

}
