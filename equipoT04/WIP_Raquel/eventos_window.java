package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import javax.swing.ImageIcon;

public class eventos_window extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					eventos_window frame = new eventos_window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public eventos_window() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(eventos_window.class.getResource("/img/imagenes/icon app.png")));
		setTitle("Eventos");
		setForeground(new Color(0, 0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 671);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel etiqueta1 = new JLabel("EVENTOS AMIGOS");
		etiqueta1.setForeground(new Color(128, 0, 128));
		etiqueta1.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueta1.setBounds(10, 11, 144, 14);
		contentPane.add(etiqueta1);
		
		JRadioButton rboton1 = new JRadioButton("RUTA SENDERISMO AMIGOS. Inscripci\u00F3n gratuita. Ruta lila de senderismo por Horta de San Joan para grupo de amigos . Ruta para el dia 26/06/16.", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton1.setBounds(10, 32, 800, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("TALLER DE CASTELLS. Inscripci\u00F3n gratuita. Taller de Castells al Serrallo el dia 26/06/16. Taller para grupo de amigos a partir de 16 a\u00F1os.", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton2.setBounds(10, 58, 800, 23);
		contentPane.add(rboton2);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		
		JLabel etiqueta2 = new JLabel("EVENTOS DEPORTIVOS");
		etiqueta2.setForeground(new Color(128, 0, 128));
		etiqueta2.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueta2.setBounds(10, 88, 185, 14);
		contentPane.add(etiqueta2);
		
		JRadioButton rboton3 = new JRadioButton("TORNEO DE FUTBOL 7. Inscripcion gratuita. Torneo de Futbol 7 en poliderportivo municipal de Valls el dia 26/06/16. Torneo para mayores de 18 a\u00F1os. ", true);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton3.setBounds(10, 109, 800, 23);
		contentPane.add(rboton3);
		
		ButtonGroup grupoBR2 = new ButtonGroup();
		grupoBR.add(rboton3);
		
		JLabel etiqueta3 = new JLabel("EVENTOS FAMILIARES");
		etiqueta3.setForeground(new Color(128, 0, 128));
		etiqueta3.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueta3.setBounds(10, 139, 167, 14);
		contentPane.add(etiqueta3);
		
		JRadioButton rboton4 = new JRadioButton("RUTA SENDERISMO FAMILIAR. Inscripci\u00F3n gratuita. Ruta verde de senderismo por Horta de San Joan para familias . Ruta para el dia 26/06/16.", true);
		rboton4.setForeground(new Color(128, 0, 128));
		rboton4.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton4.setBounds(10, 160, 800, 23);
		contentPane.add(rboton4);
		
		ButtonGroup grupoBR3 = new ButtonGroup();
		grupoBR.add(rboton4);
		
		JLabel etiqueta4 = new JLabel("EVENTOS INDIVIDUALES");
		etiqueta4.setForeground(new Color(128, 0, 128));
		etiqueta4.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueta4.setBounds(10, 190, 202, 14);
		contentPane.add(etiqueta4);
		
		JRadioButton rboton5 = new JRadioButton("RUTA SENDERISMO. Inscripci\u00F3n gratuita. Ruta roja de senderismo por Horta de San Joan. Ruta para el dia 26/06/16. Ruta para mayores de 18 a\u00F1os.", true);
		rboton5.setForeground(new Color(128, 0, 128));
		rboton5.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton5.setBounds(10, 211, 800, 23);
		contentPane.add(rboton5);
		
		ButtonGroup grupoBR4 = new ButtonGroup();
		grupoBR.add(rboton5);
		
		JLabel etiqueta5 = new JLabel("EVENTOS INFANTILES");
		etiqueta5.setForeground(new Color(128, 0, 128));
		etiqueta5.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueta5.setBounds(10, 241, 185, 14);
		contentPane.add(etiqueta5);
		
		JRadioButton rboton6 = new JRadioButton("BICICLETADA INFANTIL. Inscripci\u00F3n gratuita. Biciletada para ni\u00F1os de 5 a 12 a\u00F1os en el mercado de Bonavista a las 10:00h de la ma\u00F1ana del dia 25/06/16.", true);
		rboton6.setForeground(new Color(128, 0, 128));
		rboton6.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton6.setBounds(10, 262, 800, 23);
		contentPane.add(rboton6);
		
		JRadioButton rboton7 = new JRadioButton("TALLER DE MARIONETAS. Inscripci\u00F3n gratuita.Taller para ni\u00F1os de 5 a 12a\u00F1os en el centro civico de Bonavista a las 11h de la mana\u00F1ana el dia 26/06/16.", false);
		rboton7.setForeground(new Color(128, 0, 128));
		rboton7.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton7.setBounds(10, 288, 800, 23);
		contentPane.add(rboton7);
		
		ButtonGroup grupoBR5 = new ButtonGroup();
		grupoBR.add(rboton6);
		grupoBR.add(rboton7);
		
		JLabel etiqueta6 = new JLabel("EVENTOS JUBILADOS");
		etiqueta6.setForeground(new Color(128, 0, 128));
		etiqueta6.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueta6.setBounds(10, 318, 167, 14);
		contentPane.add(etiqueta6);
		
		JRadioButton rboton8 = new JRadioButton("RUTA SENDERISMO JUBILADOS. Inscripci\u00F3n gratuita. Ruta blanca de senderismo por Horta de San Joan. Ruta para el dia 26/06/16.", true);
		rboton8.setForeground(new Color(128, 0, 128));
		rboton8.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton8.setBounds(10, 339, 800, 23);
		contentPane.add(rboton8);
		
		JRadioButton rboton9 = new JRadioButton("AQUAGYM. Inscripci\u00F3n gratuita. Aquagym para jubilados durante toda la ma\u00F1ana del 26/06/16 en distintas sesiones.", false);
		rboton9.setForeground(new Color(128, 0, 128));
		rboton9.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton9.setBounds(10, 365, 800, 23);
		contentPane.add(rboton9);
		
		ButtonGroup grupoBR6 = new ButtonGroup();
		grupoBR.add(rboton8);
		grupoBR.add(rboton9);
		
		JLabel etiqueta7 = new JLabel("EVENTOS MASCOTAS");
		etiqueta7.setForeground(new Color(128, 0, 128));
		etiqueta7.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueta7.setBounds(10, 395, 167, 14);
		contentPane.add(etiqueta7);
		
		JRadioButton rboton10 = new JRadioButton("RUTA SENDERISMO CON MASCOTAS. Inscripci\u00F3n gratuita. Ruta azul de senderismo por Horta de San Joan con mascotas . Ruta para el dia 26/06/16.", true);
		rboton10.setForeground(new Color(128, 0, 128));
		rboton10.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton10.setBounds(10, 416, 800, 23);
		contentPane.add(rboton10);
		
		ButtonGroup grupoBR7 = new ButtonGroup();
		grupoBR.add(rboton10);
		
		JLabel etiqueat8 = new JLabel("EVENTOS PAREJAS");
		etiqueat8.setForeground(new Color(128, 0, 128));
		etiqueat8.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueat8.setBounds(10, 451, 167, 14);
		contentPane.add(etiqueat8);
		
		JRadioButton rboton11 = new JRadioButton("RUTA SENDERISMO PAREJAS. Inscripci\u00F3n gratuita. Ruta amarilla de senderismo por Horta de San Joan para parejas . Ruta para el dia 26/06/16.", true);
		rboton11.setForeground(new Color(128, 0, 128));
		rboton11.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rboton11.setBounds(6, 472, 804, 23);
		contentPane.add(rboton11);
		
		ButtonGroup grupoBR8 = new ButtonGroup();
		grupoBR.add(rboton11);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(8, 619, 146, 14);
		contentPane.add(progressBar);
		
		JLabel etiqueta9 = new JLabel("PROXIMOS EVENTOS");
		etiqueta9.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		etiqueta9.setForeground(new Color(128, 0, 128));
		etiqueta9.setBounds(10, 502, 227, 14);
		contentPane.add(etiqueta9);
		
		JLabel lblEscapaRomanticaA = new JLabel("ESCAPA ROMANTICA A PARIS ");
		lblEscapaRomanticaA.setForeground(new Color(128, 0, 128));
		lblEscapaRomanticaA.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblEscapaRomanticaA.setBounds(10, 527, 202, 14);
		contentPane.add(lblEscapaRomanticaA);
		
		JLabel lblSparelaxParaJubilados = new JLabel("SPA-RELAX PARA JUBILADOS");
		lblSparelaxParaJubilados.setForeground(new Color(128, 0, 128));
		lblSparelaxParaJubilados.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblSparelaxParaJubilados.setBounds(10, 554, 202, 14);
		contentPane.add(lblSparelaxParaJubilados);
		
		JLabel lblParquesAtraccionesY = new JLabel("PARQUES ATRACCIONES Y ACUATICOS PARA TODOS");
		lblParquesAtraccionesY.setForeground(new Color(128, 0, 128));
		lblParquesAtraccionesY.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblParquesAtraccionesY.setBounds(265, 528, 418, 14);
		contentPane.add(lblParquesAtraccionesY);
		
		JLabel lblNewLabel_3 = new JLabel("COLONIAS INFANTILES");
		lblNewLabel_3.setForeground(new Color(128, 0, 128));
		lblNewLabel_3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblNewLabel_3.setBounds(10, 579, 167, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblCampamentosParaJovenes = new JLabel("CAMPAMENTOS PARA JOVENES DE 14 A 16 A\u00D1OS");
		lblCampamentosParaJovenes.setForeground(new Color(128, 0, 128));
		lblCampamentosParaJovenes.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblCampamentosParaJovenes.setBounds(265, 554, 355, 14);
		contentPane.add(lblCampamentosParaJovenes);
		
		JLabel lblEscapadaRuralFamiliar = new JLabel("ESCAPADA RURAL FAMILIAR ");
		lblEscapadaRuralFamiliar.setForeground(new Color(128, 0, 128));
		lblEscapadaRuralFamiliar.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblEscapadaRuralFamiliar.setBounds(265, 579, 202, 14);
		contentPane.add(lblEscapadaRuralFamiliar);
		
		JButton btnReservar = new JButton("RESERVAR");
		btnReservar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eventos_w_gratis w1 = new eventos_w_gratis();
				w1.setVisible(true);
			}
		});
		btnReservar.setBackground(SystemColor.inactiveCaptionBorder);
		btnReservar.setForeground(new Color(139, 0, 0));
		btnReservar.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnReservar.setBounds(632, 559, 178, 56);
		contentPane.add(btnReservar);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setForeground(new Color(128, 0, 128));
		lblNewLabel.setIcon(new ImageIcon(eventos_window.class.getResource("/img/imagenes/p_peques (3).jpg")));
		lblNewLabel.setBounds(0, 0, 855, 633);
		contentPane.add(lblNewLabel);
	}
}
