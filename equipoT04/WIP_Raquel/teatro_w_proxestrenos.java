package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class teatro_w_proxestrenos extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					teatro_w_proxestrenos frame = new teatro_w_proxestrenos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public teatro_w_proxestrenos() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(teatro_w_proxestrenos.class.getResource("/img/imagenes/icon app.png")));
		setTitle("Pr\u00F3ximos estrenos teatro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 382, 265);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel etiqueta1 = new JLabel("Pr\u00F3ximamente en salas de teatro...");
		etiqueta1.setForeground(new Color(128, 0, 128));
		etiqueta1.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		etiqueta1.setBounds(33, 22, 307, 14);
		contentPane.add(etiqueta1);
		
		JRadioButton rboton1 = new JRadioButton("La muerte de Julia", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setBounds(33, 66, 164, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("Marionetas en la nieve", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(33, 109, 164, 23);
		contentPane.add(rboton2);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		
		JButton boton1 = new JButton("COMPRAR ENTRADAS");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		boton1.setForeground(new Color(139, 0, 0));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton1.setBounds(137, 178, 203, 38);
		contentPane.add(boton1);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(teatro_w_proxestrenos.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 366, 227);
		contentPane.add(lblNewLabel);
	}

}
