package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.Font;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class vcult_w_garte extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vcult_w_garte frame = new vcult_w_garte();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public vcult_w_garte() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(vcult_w_garte.class.getResource("/img/imagenes/icon app.png")));
		setTitle("Galer\u00EDas de Arte");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 331, 248);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JRadioButton rboton1 = new JRadioButton("Marlborough Galeria, Barcelona", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setBounds(27, 31, 203, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("Galer\u00EDa Altexerri, Bilbao", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(27, 67, 203, 23);
		contentPane.add(rboton2);
		
		JRadioButton rboton3 = new JRadioButton("Galer\u00EDa Antonia Puy\u00F3, Zaragoza\r\n", false);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton3.setBounds(27, 107, 203, 23);
		contentPane.add(rboton3);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		grupoBR.add(rboton3);
		
		JButton boton = new JButton("COMPRAR ENTRADAS");
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		boton.setForeground(new Color(139, 0, 0));
		boton.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton.setBounds(93, 163, 203, 36);
		contentPane.add(boton);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(vcult_w_garte.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 324, 210);
		contentPane.add(lblNewLabel);
	}

}
