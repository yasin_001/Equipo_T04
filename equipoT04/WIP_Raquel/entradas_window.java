package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import javax.swing.JSlider;
import java.awt.Component;
import javax.swing.ImageIcon;

public class entradas_window extends JFrame {

	private JPanel contentPane;
	private JTextField tf1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					entradas_window frame = new entradas_window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public entradas_window() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(entradas_window.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 229, 301);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u00BFCu\u00E1ntas entradas quiere?");
		lblNewLabel.setForeground(new Color(128, 0, 128));
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		lblNewLabel.setBounds(39, 11, 174, 14);
		contentPane.add(lblNewLabel);
		
		final JSpinner spinner = new JSpinner();
		spinner.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		spinner.setModel(new SpinnerNumberModel(0, 0, 50, 1));
		spinner.setBounds(10, 8, 29, 20);
		contentPane.add(spinner);
		
		JLabel lblOfertaAElegir = new JLabel("OFERTA A ELEGIR:");
		lblOfertaAElegir.setForeground(new Color(128, 0, 128));
		lblOfertaAElegir.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblOfertaAElegir.setBounds(6, 36, 154, 14);
		contentPane.add(lblOfertaAElegir);
		
		JRadioButton rboton2 = new JRadioButton("CARNET JOVEN", true);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(10, 57, 154, 23);
		contentPane.add(rboton2);
		
		JRadioButton rboton3 = new JRadioButton("JUBILADOS", false);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton3.setBounds(10, 84, 154, 23);
		contentPane.add(rboton3);
		
		JRadioButton rboton4 = new JRadioButton("PACK AMIGOS", false);
		rboton4.setForeground(new Color(128, 0, 128));
		rboton4.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton4.setBounds(10, 110, 154, 23);
		contentPane.add(rboton4);
		
		JRadioButton rboton5 = new JRadioButton("PACK FAMILIAR", false);
		rboton5.setForeground(new Color(128, 0, 128));
		rboton5.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton5.setBounds(10, 136, 154, 23);
		contentPane.add(rboton5);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton2);
		grupoBR.add(rboton3);
		grupoBR.add(rboton4);
		grupoBR.add(rboton5);
		
		tf1 = new JTextField();
		tf1.setAlignmentY(Component.TOP_ALIGNMENT);
		tf1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		tf1.setOpaque(false);
		tf1.setRequestFocusEnabled(false);
		tf1.setBounds(54, 193, 67, 35);
		contentPane.add(tf1);
		tf1.setColumns(10);
		
		
		JButton boton2 = new JButton("RESERVAR Y PAGAR");
		boton2.setForeground(new Color(139, 0, 0));
		boton2.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton2.setBounds(10, 239, 196, 23);
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inicio w1 = new Inicio();
				w1.setVisible(true);
			}
		});
		contentPane.add(boton2);
		
		
		JButton boton1 = new JButton("TOTAL");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String a = spinner.getValue().toString();
				int entradas = Integer.parseInt(a);
				tf1.setText((entradas*8)-(entradas*8*0.2)+" �");
			
					}
				
				});
				
		
		boton1.setForeground(new Color(139, 0, 0));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton1.setBounds(20, 166, 149, 20);
		contentPane.add(boton1);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(entradas_window.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel_1.setBounds(0, -12, 223, 285);
		contentPane.add(lblNewLabel_1);
		
	
	}
}
