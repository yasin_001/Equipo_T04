


package WIP_Raquel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class teatro_SQL {
	private static Connection Conexion = null;

	
	//Todos estos m�todos son VOID porque trabajamos con java y sql a la vez. As� nos dir� de d�nde viene el error (si hay)
		
		
		//M�TODO PERSONALIZADO DE CONEXION A SQL
		public void SQLConnection(String user, String pass, String db_name) throws Exception {
		        try {
		            Class.forName("oracle.jdbc.driver.OracleDriver");
		            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
		            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
		        } catch (ClassNotFoundException ex) {
		            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
		        }
		    }
		
		
		
		//M�TODO QUE FINALIZA LA CONEXION A SQL
			public void closeConnection() {
			        try {
			            Conexion.close(); //!!!!!!!!
			            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
			        } catch (SQLException ex) {
			           
			            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
			        }
			    }
			
			
			//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
			 public void createTable(String name) {
			        try {
			            String Query = "CREATE TABLE " + name + ""
			                    + "(ID_TEATRO VARCHAR2(100) PRIMARY KEY, LOCALIDAD VARCHAR2(100), PRECIO NUMBER(5), UBICACION VARCHAR2(1000), OFERTA VARCHAR2(1000), NOMBRE_FT VARCHAR2(100))";
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
			        } catch (SQLException ex) {
			            Logger.getLogger(cine_SQL.class.getName()).log(Level.SEVERE, null, ex);
			        }
			    }
			 
			 
			//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
			 public void insertData(String table_name, String ID_TEATRO, String LOCALIDAD, int PRECIO, String UBICACION, String OFERTA, String NOMBRE_FT) {
			        try {
			            String Query = "INSERT INTO " + table_name + " VALUES("
			            		+ "'"+ ID_TEATRO + "',"
			            		+ "'"+ LOCALIDAD + "',"
			            		+ "'"+ PRECIO + "',"
			            		+ "'"+ UBICACION + "',"
			            		+ "'"+ OFERTA + "',"
			            		+ "'"+ NOMBRE_FT + "')";
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			        }
			    }	
			
			 
			//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
			 public void getValues(String table_name) {
			        try {
			            String Query = "SELECT * FROM " + table_name;
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);

			            while (resultSet.next()) {  
			                System.out.println("ID_TEATRO: " + resultSet.getString("ID_TEATRO") + " "
			                        + "Localidad: " + resultSet.getString("LOCALIDAD") + " " 
									+ "Precio: " + resultSet.getInt("PRECIO") + " "
			                        + "Ubicacion: " + resultSet.getString("UBICACION") + " "
			                        + "Oferta: " + resultSet.getString("OFERTA")
			                        + "Nombre pelicula: " + resultSet.getString("NOMBRE_FT") + " ");
			            }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
			    }	
			 
			 
			//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
			 public void deleteRecord(String table_name, String ID_TEATRO) {
			        try {
			            String Query = "DELETE FROM " + table_name + " WHERE ID = '" + ID_TEATRO + "'";
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
			        }
			    }
}
