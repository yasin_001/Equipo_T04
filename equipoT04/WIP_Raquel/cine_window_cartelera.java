package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class cine_window_cartelera extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cine_window_cartelera frame = new cine_window_cartelera();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public cine_window_cartelera() {
		setTitle("Cartelera cine");
		setIconImage(Toolkit.getDefaultToolkit().getImage(cine_window_cartelera.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 386, 263);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel etiqueta1 = new JLabel("Esta semana en cartelera tenemos...");
		etiqueta1.setForeground(new Color(128, 0, 128));
		etiqueta1.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		etiqueta1.setBounds(10, 11, 298, 29);
		contentPane.add(etiqueta1);
		
		JRadioButton rboton1 = new JRadioButton("Capit\u00E1n Am\u00E9rica", true);
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setBounds(20, 59, 126, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("La bruja", false);
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setBounds(20, 95, 126, 23);
		contentPane.add(rboton2);
		
		JRadioButton rboton3 = new JRadioButton("El terror de...", false);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton3.setBounds(21, 135, 125, 23);
		contentPane.add(rboton3);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		grupoBR.add(rboton3);
		
		JButton boton = new JButton("COMPRAR ENTRADAS\r\n");
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		boton.setForeground(new Color(139, 0, 0));
		boton.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton.setBounds(172, 167, 184, 43);
		contentPane.add(boton);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(cine_window_cartelera.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 370, 225);
		contentPane.add(lblNewLabel);
	}
}
