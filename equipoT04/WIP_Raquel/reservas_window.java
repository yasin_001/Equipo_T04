package WIP_Raquel;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class reservas_window extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					reservas_window frame = new reservas_window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public reservas_window() {
		setTitle("Reservas");
		setIconImage(Toolkit.getDefaultToolkit().getImage(reservas_window.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 487, 157);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Su/s entrada/s ya est\u00E1n reservadas y enviadas a su email.");
		lblNewLabel.setForeground(new Color(128, 0, 128));
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblNewLabel.setBounds(20, 11, 399, 55);
		contentPane.add(lblNewLabel);
		
		JButton btnVolverAInicio = new JButton("VOLVER A INICIO");
		btnVolverAInicio.setForeground(new Color(139, 0, 0));
		btnVolverAInicio.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnVolverAInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
						Inicio w1 = new Inicio();
						w1.setVisible(true);
					
			}
		});
		btnVolverAInicio.setBounds(251, 77, 198, 23);
		contentPane.add(btnVolverAInicio);
		
		JLabel lblNewLabel_1 = new JLabel("Gracias por usar nuestra app.");
		lblNewLabel_1.setForeground(new Color(128, 0, 128));
		lblNewLabel_1.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		lblNewLabel_1.setBounds(20, 77, 185, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon(reservas_window.class.getResource("/img/imagenes/p_peques (4).jpg")));
		lblNewLabel_2.setBounds(0, 0, 471, 119);
		contentPane.add(lblNewLabel_2);
	}

}
