package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class cine_w_proxestrenos extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cine_w_proxestrenos frame = new cine_w_proxestrenos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public cine_w_proxestrenos() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(cine_w_proxestrenos.class.getResource("/img/imagenes/icon app.png")));
		setTitle("Pr\u00F3ximos estrenos cine");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 386, 283);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLaSemanaQue = new JLabel("La semana que viene estrenamos...");
		lblLaSemanaQue.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		lblLaSemanaQue.setForeground(new Color(128, 0, 128));
		lblLaSemanaQue.setBounds(21, 11, 274, 36);
		contentPane.add(lblLaSemanaQue);
		
		JRadioButton rboton1 = new JRadioButton("Los cuentos del Cuenta Cuentos", true);
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setBounds(35, 72, 244, 25);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("Fantas\u00EDa 2016", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(35, 118, 244, 23);
		contentPane.add(rboton2);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		
		
		JButton btnComprarEntradas = new JButton("COMPRAR ENTRADAS");
		btnComprarEntradas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		btnComprarEntradas.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnComprarEntradas.setForeground(new Color(139, 0, 0));
		btnComprarEntradas.setBounds(149, 181, 181, 41);
		contentPane.add(btnComprarEntradas);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(cine_w_proxestrenos.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 378, 251);
		contentPane.add(lblNewLabel);
	}

}
