package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class cine_w_prox extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cine_w_prox frame = new cine_w_prox();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public cine_w_prox() {
		setTitle("Pr\u00F3ximamente en cine...");
		setIconImage(Toolkit.getDefaultToolkit().getImage(cine_w_prox.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 380, 283);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPrximamenteEnCine = new JLabel("Pr\u00F3ximamente en cine...");
		lblPrximamenteEnCine.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		lblPrximamenteEnCine.setForeground(new Color(128, 0, 128));
		lblPrximamenteEnCine.setBounds(10, 11, 234, 36);
		contentPane.add(lblPrximamenteEnCine);
		
		JRadioButton rboton1 = new JRadioButton("Los hombres de negro", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setBounds(26, 66, 160, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("Las aventuras de John", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(26, 108, 160, 23);
		contentPane.add(rboton2);
		
		JRadioButton rboton3 = new JRadioButton("Guerra Mundial IV", false);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton3.setBounds(26, 152, 160, 23);
		contentPane.add(rboton3);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		grupoBR.add(rboton3);
		
		JButton boton1 = new JButton("COMPRAR ENTRADAS");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		boton1.setForeground(new Color(139, 0, 0));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton1.setBounds(131, 198, 202, 36);
		contentPane.add(boton1);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(cine_w_prox.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 373, 245);
		contentPane.add(lblNewLabel);
	}

}
