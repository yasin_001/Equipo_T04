package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.Font;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class vcult_w_museos extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vcult_w_museos frame = new vcult_w_museos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public vcult_w_museos() {
		setTitle("Museos");
		setIconImage(Toolkit.getDefaultToolkit().getImage(vcult_w_museos.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 372, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JRadioButton rboton1 = new JRadioButton("Museu de les Arts i les Ci\u00E8ncies, Valencia", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setBounds(21, 34, 289, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("Museo Arqueol\u00F3gico, Sevilla", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(21, 62, 289, 23);
		contentPane.add(rboton2);
		
		JRadioButton rboton3 = new JRadioButton("Museo W\u00FCrth, La Rioja", false);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton3.setBounds(21, 88, 289, 23);
		contentPane.add(rboton3);
		
		JRadioButton rboton4 = new JRadioButton("Museo Guggenheim, Bilbao", false);
		rboton4.setForeground(new Color(128, 0, 128));
		rboton4.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton4.setBounds(21, 114, 289, 23);
		contentPane.add(rboton4);
		
		JRadioButton rboton5 = new JRadioButton("Museu d'Art Contemporani MACBA, Barcelona", false);
		rboton5.setForeground(new Color(128, 0, 128));
		rboton5.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton5.setBounds(21, 143, 289, 23);
		contentPane.add(rboton5);
		
		JRadioButton rboton6 = new JRadioButton("EL Prado, Madrid", false);
		rboton6.setForeground(new Color(128, 0, 128));
		rboton6.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton6.setBounds(21, 169, 289, 23);
		contentPane.add(rboton6);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		grupoBR.add(rboton3);
		grupoBR.add(rboton4);
		grupoBR.add(rboton5);
		grupoBR.add(rboton6);
		
		JButton boton = new JButton("COMPRAR ENTRADAS");
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		boton.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton.setForeground(new Color(139, 0, 0));
		boton.setBounds(122, 213, 188, 38);
		contentPane.add(boton);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(vcult_w_museos.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 356, 262);
		contentPane.add(lblNewLabel);
	}

	public static void DISPOSE_ON_CLOSE() {
		// TODO Auto-generated method stub
		
	}

}
