package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JMenuBar;
import java.awt.Color;
import javax.swing.JProgressBar;
import javax.swing.JPasswordField;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.SystemColor;




public class Inicio extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JTextField tf_usuario;
	private JPasswordField pf_contra;
	private JTextField tf_email;
	private JTextField tf_tlf;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio frame = new Inicio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Inicio() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Inicio.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnEntrar = new JButton("ENTRAR");
		btnEntrar.setForeground(new Color(128, 0, 128));
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inicio w1 = new Inicio();
				w1.setVisible(true);
			}
		});
		btnEntrar.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnEntrar.setBounds(72, 440, 150, 23);
		contentPane.add(btnEntrar);
		
		JButton btnRegistrarse = new JButton("REGISTRARSE");
		btnRegistrarse.setForeground(new Color(128, 0, 128));
		btnRegistrarse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					
					login_SQL usuario1 = new login_SQL();
					
					usuario1.SQLConnection("proyecto", "root", "");
					
					usuario1.createTable("Usuarios");
					usuario1.insertData("Usuarios", " "+tf_usuario.getText(), " "+pf_contra.getPassword(), " "+tf_email.getText(), " "+tf_tlf.getText());
					Inicio w1 = new Inicio();
					w1.setVisible(true);
					usuario1.closeConnection();
					
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnRegistrarse.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnRegistrarse.setBounds(596, 440, 150, 23);
		contentPane.add(btnRegistrarse);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(10, 607, 146, 14);
		contentPane.add(progressBar);
		
		JLabel label = new JLabel("Usuario:");
		label.setForeground(new Color(255, 255, 224));
		label.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		label.setBounds(31, 215, 111, 31);
		contentPane.add(label);
		
		textField = new JTextField();
		textField.setBounds(136, 223, 170, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setForeground(new Color(255, 255, 224));
		lblContrasea.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		lblContrasea.setBounds(10, 287, 111, 31);
		contentPane.add(lblContrasea);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(139, 295, 167, 20);
		contentPane.add(passwordField);
		
		JLabel label_2 = new JLabel("Contrase\u00F1a:");
		label_2.setForeground(new Color(128, 0, 128));
		label_2.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		label_2.setBounds(416, 261, 111, 31);
		contentPane.add(label_2);
		
		tf_usuario = new JTextField();
		tf_usuario.setBounds(570, 223, 203, 20);
		contentPane.add(tf_usuario);
		tf_usuario.setColumns(10);
		
		pf_contra = new JPasswordField();
		pf_contra.setBounds(570, 269, 203, 20);
		contentPane.add(pf_contra);
		
		tf_email = new JTextField();
		tf_email.setBounds(570, 324, 203, 20);
		contentPane.add(tf_email);
		tf_email.setColumns(10);
		
		JLabel lblolvidasteLaContrasea = new JLabel("\u00BFOlvidaste la contrase\u00F1a?");
		lblolvidasteLaContrasea.setForeground(new Color(255, 255, 224));
		lblolvidasteLaContrasea.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		lblolvidasteLaContrasea.setBounds(42, 358, 264, 31);
		contentPane.add(lblolvidasteLaContrasea);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setForeground(new Color(128, 0, 128));
		lblEmail.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		lblEmail.setBounds(416, 316, 111, 31);
		contentPane.add(lblEmail);
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono:");
		lblTelfono.setForeground(new Color(128, 0, 128));
		lblTelfono.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		lblTelfono.setBounds(416, 379, 111, 31);
		contentPane.add(lblTelfono);
		
		tf_tlf = new JTextField();
		tf_tlf.setBounds(569, 387, 204, 20);
		contentPane.add(tf_tlf);
		tf_tlf.setColumns(10);
		
		JButton btnNewButton = new JButton("Entra y sigue con la compra");
		btnNewButton.setForeground(new Color(128, 0, 128));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reservas_window w1 = new reservas_window();
				w1.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		btnNewButton.setBounds(31, 508, 234, 43);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Reg\u00EDstrate y sigue con la compra");
		btnNewButton_1.setForeground(new Color(128, 0, 128));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Reserva1 w1 = new Reserva1();
				w1.setVisible(true);
			}
		});
		btnNewButton_1.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		btnNewButton_1.setBounds(507, 508, 283, 43);
		contentPane.add(btnNewButton_1);
		
		JLabel label_1 = new JLabel("Usuario:");
		label_1.setBackground(SystemColor.inactiveCaptionBorder);
		label_1.setForeground(new Color(128, 0, 128));
		label_1.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		label_1.setBounds(416, 215, 111, 31);
		contentPane.add(label_1);
		
		JButton btncine = new JButton("CINE");
		btncine.setForeground(new Color(128, 0, 128));
		btncine.setBounds(0, 11, 105, 31);
		contentPane.add(btncine);
		btncine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cine_window w1 = new cine_window();
				w1.setVisible(true);
			}
		});
		btncine.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btnteatro = new JButton("TEATRO");
		btnteatro.setForeground(new Color(128, 0, 128));
		btnteatro.setBounds(530, 11, 111, 31);
		contentPane.add(btnteatro);
		btnteatro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				teatro_window w1 = new teatro_window();
				w1.setVisible(true);
			}
		});
		btnteatro.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btnconciertos = new JButton("CONCIERTOS");
		btnconciertos.setForeground(new Color(128, 0, 128));
		btnconciertos.setBounds(104, 11, 161, 31);
		contentPane.add(btnconciertos);
		btnconciertos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				conciertos_window2 w1 = new conciertos_window2();
				w1.setVisible(true);
			}
		});
		btnconciertos.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btndeportes = new JButton("DEPORTES");
		btndeportes.setForeground(new Color(128, 0, 128));
		btndeportes.setBounds(264, 11, 141, 31);
		contentPane.add(btndeportes);
		btndeportes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deportes_window w1 = new deportes_window();
				w1.setVisible(true);
			}
		});
		btndeportes.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btnrurales = new JButton("VISITAS CULTURALES");
		btnrurales.setForeground(new Color(128, 0, 128));
		btnrurales.setBounds(635, 11, 219, 31);
		contentPane.add(btnrurales);
		btnrurales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vculturales_window w1 = new vculturales_window();
				w1.setVisible(true);
			}
		});
		btnrurales.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btneventos = new JButton("EVENTOS");
		btneventos.setForeground(new Color(128, 0, 128));
		btneventos.setBounds(403, 11, 128, 31);
		contentPane.add(btneventos);
		btneventos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eventos_window w1 = new eventos_window();
				w1.setVisible(true);
			}
		});
		btneventos.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon(Inicio.class.getResource("/img/imagenes/plumas (2).jpg")));
		lblNewLabel_2.setBounds(0, 0, 854, 632);
		contentPane.add(lblNewLabel_2);
		this.setSize(870, 670);
		
		ImageIcon fot = new ImageIcon();
		Component jLabel1;
		//Icon icono = new ImageIcon(fot.getImage().getScaledInstance(jLabel1.getWidth(), jLabel1.getHeight(), Image.SCALE_DEFAULT));
		//jLabel1.setIcon(icono);
		this.repaint();
	}
}
