package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class deportes_futbol_w extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					deportes_futbol_w frame = new deportes_futbol_w();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public deportes_futbol_w() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(deportes_futbol_w.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 379, 263);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEntradasFtbol = new JLabel("Entradas f\u00FAtbol");
		lblEntradasFtbol.setForeground(new Color(128, 0, 128));
		lblEntradasFtbol.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		lblEntradasFtbol.setBounds(10, 11, 125, 27);
		contentPane.add(lblEntradasFtbol);
		
		JRadioButton rboton1 = new JRadioButton("LaLiga: FC Barcelona - Real Madrid C.F. 23/04/2017", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		rboton1.setBounds(6, 58, 338, 27);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("LaLiga2: Gimn\u00E0stic - Reus F.C. 10/09/2016", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		rboton2.setBounds(6, 101, 338, 23);
		contentPane.add(rboton2);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		
		JButton btnNewButton = new JButton("COMPRAR ENTRADAS");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		btnNewButton.setForeground(new Color(139, 0, 0));
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnNewButton.setBounds(66, 161, 198, 39);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(deportes_futbol_w.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, -1, 370, 226);
		contentPane.add(lblNewLabel);
	}
}
