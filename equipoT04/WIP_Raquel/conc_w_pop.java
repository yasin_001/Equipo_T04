package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class conc_w_pop extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					conc_w_pop frame = new conc_w_pop();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public conc_w_pop() {
		setTitle("Conciertos POP");
		setIconImage(Toolkit.getDefaultToolkit().getImage(conc_w_pop.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 383, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblConciertosPop = new JLabel("CONCIERTOS POP");
		lblConciertosPop.setForeground(new Color(128, 0, 128));
		lblConciertosPop.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		lblConciertosPop.setBounds(32, 30, 177, 23);
		contentPane.add(lblConciertosPop);
		
		JRadioButton rboton1 = new JRadioButton("Alejandro Sanz", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setBounds(30, 75, 109, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("Pablo L\u00F3pez", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(30, 101, 109, 23);
		contentPane.add(rboton2);
		
		JRadioButton rboton3 = new JRadioButton("Mal\u00FA", false);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton3.setBounds(32, 127, 109, 23);
		contentPane.add(rboton3);
		
		JRadioButton rboton4 = new JRadioButton("David Bisbal\r\n", false);
		rboton4.setForeground(new Color(128, 0, 128));
		rboton4.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton4.setBounds(32, 153, 109, 23);
		contentPane.add(rboton4);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		grupoBR.add(rboton3);
		grupoBR.add(rboton4);
		
		JButton btnNewButton = new JButton("COMPRAR ENTRADAS");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		btnNewButton.setForeground(new Color(139, 0, 0));
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnNewButton.setBounds(149, 204, 196, 35);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(conc_w_pop.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 375, 262);
		contentPane.add(lblNewLabel);
	}

}
