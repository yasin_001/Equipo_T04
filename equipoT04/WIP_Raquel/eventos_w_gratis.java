package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class eventos_w_gratis extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					eventos_w_gratis frame = new eventos_w_gratis();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public eventos_w_gratis() {
		setTitle("Reserva gratuita");
		setIconImage(Toolkit.getDefaultToolkit().getImage(eventos_w_gratis.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 328, 166);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JSpinner cont1 = new JSpinner();
		cont1.setModel(new SpinnerNumberModel(0, 0, 50, 1));
		cont1.setBounds(213, 16, 38, 28);
		contentPane.add(cont1);
		
		JLabel etiqueta1 = new JLabel("\u00BFCu\u00E1ntas reservas quiere?");
		etiqueta1.setForeground(new Color(128, 0, 128));
		etiqueta1.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		etiqueta1.setBounds(38, 17, 172, 27);
		contentPane.add(etiqueta1);
		
		JButton boton1 = new JButton("RESERVAR");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inicio w1 = new Inicio();
				w1.setVisible(true);
			}
		});
		boton1.setBackground(new Color(250, 235, 215));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton1.setForeground(new Color(139, 0, 0));
		boton1.setBounds(71, 82, 159, 35);
		contentPane.add(boton1);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(eventos_w_gratis.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 312, 128);
		contentPane.add(lblNewLabel);
	}
}
