package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import WIP_Raquel.Inicio;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class Reserva1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Reserva1 frame = new Reserva1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Reserva1() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Reserva1.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 619, 156);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ya est\u00E1 registrado.");
		lblNewLabel.setForeground(new Color(128, 0, 128));
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblNewLabel.setBounds(10, 11, 459, 24);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Sus datos y su/s entrada/s o inscripion/es realizadas ya est\u00E1n reservadas y enviadas a su email.");
		lblNewLabel_1.setForeground(new Color(128, 0, 128));
		lblNewLabel_1.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		lblNewLabel_1.setBounds(10, 46, 582, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblGraciasPorUsar = new JLabel("Gracias por usar nuestra app.");
		lblGraciasPorUsar.setForeground(new Color(128, 0, 128));
		lblGraciasPorUsar.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblGraciasPorUsar.setBounds(10, 82, 161, 14);
		contentPane.add(lblGraciasPorUsar);
		
		JButton btnVolverAInicio = new JButton("VOLVER A INICIO");
		btnVolverAInicio.setForeground(new Color(139, 0, 0));
		btnVolverAInicio.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		btnVolverAInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inicio inicio = new Inicio();
				inicio.setVisible(true);
			}
		});
		btnVolverAInicio.setBounds(385, 84, 207, 23);
		contentPane.add(btnVolverAInicio);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon(Reserva1.class.getResource("/img/imagenes/p_peques (4).jpg")));
		lblNewLabel_2.setBounds(0, 0, 603, 118);
		contentPane.add(lblNewLabel_2);
	}

}
