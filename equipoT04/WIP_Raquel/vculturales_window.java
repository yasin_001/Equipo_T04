
package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class vculturales_window extends JFrame {

	private JPanel panel_vculturales;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vculturales_window frame = new vculturales_window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	 


	/**
	 * Create the frame.
	 */
	public vculturales_window() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(vculturales_window.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 671);
		panel_vculturales = new JPanel();
		panel_vculturales.setBackground(new Color(255, 255, 255));
		panel_vculturales.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel_vculturales);
		panel_vculturales.setLayout(null);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(0, 619, 146, 14);
		panel_vculturales.add(progressBar);
		
		JButton boton1 = new JButton("Museos");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vcult_w_museos w1 = new vcult_w_museos();
				w1.setVisible(true);
				
			}
		});
		boton1.setForeground(new Color(128, 0, 128));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		boton1.setBounds(0, 11, 192, 35);
		panel_vculturales.add(boton1);
		
		JButton boton2 = new JButton("Galer\u00EDas de Arte");
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vcult_w_garte w1 = new vcult_w_garte();
				w1.setVisible(true);
			}
		});
		boton2.setForeground(new Color(128, 0, 128));
		boton2.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		boton2.setBounds(0, 57, 192, 35);
		panel_vculturales.add(boton2);
		
		JButton boton3 = new JButton("Exposiciones");
		boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vcult_w_expo w1 = new vcult_w_expo();
				w1.setVisible(true);
			}
		});
		boton3.setForeground(new Color(128, 0, 128));
		boton3.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		boton3.setBounds(0, 103, 192, 35);
		panel_vculturales.add(boton3);
		
		JButton boton4 = new JButton("Ofertas");
		boton4.setForeground(new Color(128, 0, 128));
		boton4.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		boton4.setBounds(0, 149, 192, 35);
		panel_vculturales.add(boton4);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(vculturales_window.class.getResource("/img/imagenes/trexmuseo (2).jpg")));
		lblNewLabel.setBounds(0, 0, 855, 633);
		panel_vculturales.add(lblNewLabel);
	}
}
