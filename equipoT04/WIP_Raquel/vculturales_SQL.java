package WIP_Raquel;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;



public class vculturales_SQL {


		private static Connection Conexion = null;

			
			
		//M�TODO PERSONALIZADO DE CONEXION A SQL
		public void SQLConnection(String user, String pass, String db_name) throws Exception {
		        try {
		            Class.forName("oracle.jdbc.driver.OracleDriver");
		            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
		            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
		        } catch (ClassNotFoundException ex) {
		            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
		        }
		    }
	         
			
		//M�TODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }
		
				
				//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
				 public void createTable(String name) {
				        try {
				            String Query = "CREATE TABLE " + name + ""
				                    + "(ID_VC VARCHAR2(100), LOCALIDAD VARCHAR2(100), PRECIO NUMBER(5), UBICACION VARCHAR2(1000), OFERTA VARCHAR2(1000), NOMBRE VARCHAR2(100), "
				                    + "DURACION VARCHAR2(100), EDAD VARCHAR2(50), OBS VARCHAR2(1000), TIPO VARCHAR2(100), HORA VARCHAR2(8), FECHA VARCHAR(15))";
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);
				            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
				        } catch (SQLException ex) {
				            Logger.getLogger(cine_SQL.class.getName()).log(Level.SEVERE, null, ex);
				        }
				    }
				 
				 
				//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
				 public void insertData(String table_name, String ID_VC, String LOCALIDAD, int PRECIO, String UBICACION, String OFERTA, String NOMBRE, String DURACION, String EDAD, String OBS,
						 String TIPO, String HORA, String FECHA) {
				        try {
				            String Query = "INSERT INTO " + table_name + " VALUES("
				            		+ "'"+ ID_VC + "',"
				            		+ "'"+ LOCALIDAD + "',"
				            		+ "'"+ PRECIO + "',"
				            		+ "'"+ UBICACION + "',"
				            		+ "'"+ OFERTA + "',"
				            		+ "'"+ NOMBRE + "',"
				            		+ "'"+ DURACION + "',"
				            		+ "'"+ EDAD + "',"
				            		+ "'"+ OBS + "',"
				            		+ "'"+ TIPO + "',"
				            		+ "'"+ HORA + "',"
				            		+ "'"+ FECHA + "')";
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);
				            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
				        }
				    }	
				
				 
				//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
				 public void getValues(String table_name) {
				        try {
				            String Query = "SELECT * FROM " + table_name;
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);

				            while (resultSet.next()) {  
				                System.out.println("ID_VC: " + resultSet.getString("ID_VC") + " "
				                        + "Localidad: " + resultSet.getString("LOCALIDAD") + " " 
										+ "Precio: " + resultSet.getInt("PRECIO") + " "
				                        + "Ubicacion: " + resultSet.getString("UBICACION") + " "
				                        + "Oferta: " + resultSet.getString("OFERTA") + " "
				                        + "Nombre: " + resultSet.getString("NOMBRE") + " "
				                        + "Duracion: " + resultSet.getString("DURACION") + " "
				                        + "Edad: " + resultSet.getString("EDAD") + " "
				                        + "Tipo: " + resultSet.getString("TIPO") + " "
				                        + "Hora: " + resultSet.getString("HORA") + " "
				                        + "Fecha: " + resultSet.getString("FECHA") + " ");
				            }

				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
				        }
				    }	
				 
				 
				//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
				 public void deleteRecord(String table_name, String ID_VC) {
				        try {
				            String Query = "DELETE FROM " + table_name + " WHERE ID = '" + ID_VC + "'";
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);

				        } catch (SQLException ex) {
				            System.out.println(ex.getMessage());
				            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
				        }
				    }
	}


