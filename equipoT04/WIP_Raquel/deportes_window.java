package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;

public class deportes_window extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					deportes_window frame = new deportes_window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public deportes_window() {
		setTitle("Deportes");
		setIconImage(Toolkit.getDefaultToolkit().getImage(deportes_window.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 670);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnFutbol = new JButton("FUTBOL");
		btnFutbol.setForeground(new Color(245, 222, 179));
		btnFutbol.setBackground(new Color(139, 69, 19));
		btnFutbol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
						deportes_futbol_w w1 = new deportes_futbol_w();
						w1.setVisible(true);
					}
				
			
		});
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(10, 618, 146, 14);
		contentPane.add(progressBar);
		btnFutbol.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnFutbol.setBounds(10, 11, 171, 31);
		contentPane.add(btnFutbol);
		
		JButton btnRugby = new JButton("RUGBY");
		btnRugby.setForeground(new Color(245, 222, 179));
		btnRugby.setBackground(new Color(139, 69, 19));
		btnRugby.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnRugby.setBounds(191, 53, 171, 31);
		contentPane.add(btnRugby);
		
		JButton btnNewButton = new JButton("TENNIS");
		btnNewButton.setForeground(new Color(245, 222, 179));
		btnNewButton.setBackground(new Color(139, 69, 19));
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton.setBounds(553, 11, 171, 31);
		contentPane.add(btnNewButton);
		
		JButton btnBmx = new JButton("BMX");
		btnBmx.setBackground(new Color(139, 69, 19));
		btnBmx.setForeground(new Color(245, 222, 179));
		btnBmx.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnBmx.setBounds(191, 11, 171, 31);
		contentPane.add(btnBmx);
		
		JButton btnBasquet = new JButton("BASQUET");
		btnBasquet.setForeground(new Color(245, 222, 179));
		btnBasquet.setBackground(new Color(139, 69, 19));
		btnBasquet.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnBasquet.setBounds(10, 53, 171, 31);
		contentPane.add(btnBasquet);
		
		JButton btnGolf = new JButton("GOLF");
		btnGolf.setForeground(new Color(245, 222, 179));
		btnGolf.setBackground(new Color(139, 69, 19));
		btnGolf.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnGolf.setBounds(372, 11, 171, 31);
		contentPane.add(btnGolf);
		
		JButton btnOtrosDeportes = new JButton("OTROS");
		btnOtrosDeportes.setForeground(new Color(245, 222, 179));
		btnOtrosDeportes.setBackground(new Color(139, 69, 19));
		btnOtrosDeportes.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnOtrosDeportes.setBounds(734, 11, 96, 73);
		contentPane.add(btnOtrosDeportes);
		
		JButton btnMotos = new JButton("MOTOS");
		btnMotos.setForeground(new Color(245, 222, 179));
		btnMotos.setBackground(new Color(139, 69, 19));
		btnMotos.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnMotos.setBounds(372, 53, 171, 31);
		contentPane.add(btnMotos);
		
		JButton btnCoches = new JButton("COCHES");
		btnCoches.setForeground(new Color(245, 222, 179));
		btnCoches.setBackground(new Color(139, 69, 19));
		btnCoches.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnCoches.setBounds(553, 53, 171, 31);
		contentPane.add(btnCoches);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(deportes_window.class.getResource("/img/imagenes/deportesfondo (2).jpg")));
		lblNewLabel.setBounds(0, 0, 854, 632);
		contentPane.add(lblNewLabel);
	}
}
