package WIP_Raquel;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class teatro_w_estrenos extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					teatro_w_estrenos frame = new teatro_w_estrenos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public teatro_w_estrenos() {
		setTitle("Estrenos teatro");
		setIconImage(Toolkit.getDefaultToolkit().getImage(teatro_w_estrenos.class.getResource("/img/imagenes/icon app.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 382, 282);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLaSemanaQue = new JLabel("La semana que viene estrenamos...");
		lblLaSemanaQue.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		lblLaSemanaQue.setForeground(new Color(128, 0, 128));
		lblLaSemanaQue.setBounds(36, 11, 293, 26);
		contentPane.add(lblLaSemanaQue);
		
		JRadioButton rboton1 = new JRadioButton("La comedia de tu vida", true);
		rboton1.setForeground(new Color(128, 0, 128));
		rboton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton1.setBounds(29, 56, 244, 23);
		contentPane.add(rboton1);
		
		JRadioButton rboton2 = new JRadioButton("Los vecinos del quinto", false);
		rboton2.setForeground(new Color(128, 0, 128));
		rboton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton2.setBounds(29, 92, 244, 23);
		contentPane.add(rboton2);
		
		JRadioButton rboton3 = new JRadioButton("\u00BFQui\u00E9n anda ah\u00ED?", false);
		rboton3.setForeground(new Color(128, 0, 128));
		rboton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		rboton3.setBounds(29, 131, 244, 23);
		contentPane.add(rboton3);
		
		ButtonGroup grupoBR = new ButtonGroup();
		grupoBR.add(rboton1);
		grupoBR.add(rboton2);
		grupoBR.add(rboton3);
		
		
		JButton boton1 = new JButton("COMRPAR ENTRADAS");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entradas_window w1 = new entradas_window();
				w1.setVisible(true);
			}
		});
		boton1.setForeground(new Color(139, 0, 0));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		boton1.setBounds(143, 198, 213, 35);
		contentPane.add(boton1);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(teatro_w_estrenos.class.getResource("/img/imagenes/p_peques (2).jpg")));
		lblNewLabel.setBounds(0, 0, 366, 244);
		contentPane.add(lblNewLabel);
	}
}
