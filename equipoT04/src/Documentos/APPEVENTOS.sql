HEAD;
/*EVENTOS*/
CREATE TABLE PELICULAS(
          NOMBRE_P      VARCHAR2(100) PRIMARY KEY,
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          RESUMEN       VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
          
);
CREATE TABLE CINE(
          ID_CINE       VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE_P      VARCHAR2(100),
          
          CONSTRAINT CINE_FK FOREIGN KEY (NOMBRE_P) REFERENCES PELICULAS (NOMBRE_P)
);

CREATE TABLE FUNC(
          NOMBRE_FC     VARCHAR2(100) PRIMARY KEY,
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE CONCIERTOS(
          ID_CONC       VARCHAR2(100),
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE_FC     VARCHAR2(100),
          
          CONSTRAINT CONCIERTOS_FK FOREIGN KEY (NOMBRE_FC) REFERENCES FUNC (NOMBRE_FC)
          
);

CREATE TABLE VISITAS_CULTURALES(
          ID_VC         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(100),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE FUNT(
          NOMBRE_FT     VARCHAR2(100) PRIMARY KEY,
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          RESUMEN       VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE 
);

CREATE TABLE TEATRO(
          ID_T          VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE_FT     VARCHAR2(100),
          
          CONSTRAINT TEATRO_FK FOREIGN KEY (NOMBRE_FT) REFERENCES FUNT (NOMBRE_FT)
);

CREATE TABLE FUND(
          NOMBRE_FD     VARCHAR2(100) PRIMARY KEY,
          DURACION      VARCHAR2(10),
          TIPO2         VARCHAR2(50),
          OBS           VARCHAR2(1000),
          HORA          VARCHAR2(6),
          FECHA         DATE 
);

CREATE TABLE DEPORTES(
          ID_D          VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          NOMBRE_FD     VARCHAR2(100),
          
          CONSTRAINT DEPORTES_FK FOREIGN KEY (NOMBRE_FD) REFERENCES FUND (NOMBRE_FD)
);


CREATE TABLE EVENTOS_JUBILADOS(
          ID_EJ         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_PAREJAS(
          ID_EP         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
); 

CREATE TABLE EVENTOS_FAMILIARES(
          ID_EF         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_INFANTILES(
          ID_EI         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_AMIGOS(
          ID_EA         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_MASCOTAS(
          ID_EM         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_DEPORTIVOS(
          ID_ED         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_INDIVIDUALES(
          ID_EIN         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);
=======
/*EVENTOS*/
CREATE TABLE PELICULAS(
          NOMBRE_P      VARCHAR2(100) PRIMARY KEY,
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          RESUMEN       VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
          
);
CREATE TABLE CINE(
          ID_CINE       VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE_P      VARCHAR2(100),
          
          CONSTRAINT CINE_FK FOREIGN KEY (NOMBRE_P) REFERENCES PELICULAS (NOMBRE_P)
);

CREATE TABLE FUNC(
          NOMBRE_FC     VARCHAR2(100) PRIMARY KEY,
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE CONCIERTOS(
          ID_CONC       VARCHAR2(100),
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE_FC     VARCHAR2(100),
          
          CONSTRAINT CONCIERTOS_FK FOREIGN KEY (NOMBRE_FC) REFERENCES FUNC (NOMBRE_FC)
          
);

CREATE TABLE VISITAS_CULTURALES(
          ID_VC         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(100),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE FUNT(
          NOMBRE_FT     VARCHAR2(100) PRIMARY KEY,
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          RESUMEN       VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE 
);

CREATE TABLE TEATRO(
          ID_T          VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE_FT     VARCHAR2(100),
          
          CONSTRAINT TEATRO_FK FOREIGN KEY (NOMBRE_FT) REFERENCES FUNT (NOMBRE_FT)
);

CREATE TABLE FUND(
          NOMBRE_FD     VARCHAR2(100) PRIMARY KEY,
          DURACION      VARCHAR2(10),
          TIPO2         VARCHAR2(50),
          OBS           VARCHAR2(1000),
          HORA          VARCHAR2(6),
          FECHA         DATE 
);

CREATE TABLE DEPORTES(
          ID_D          VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          NOMBRE_FD     VARCHAR2(100),
          
          CONSTRAINT DEPORTES_FK FOREIGN KEY (NOMBRE_FD) REFERENCES FUND (NOMBRE_FD)
);


CREATE TABLE EVENTOS_JUBILADOS(
          ID_EJ         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_PAREJAS(
          ID_EP         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
); 

CREATE TABLE EVENTOS_FAMILIARES(
          ID_EF         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_INFANTILES(
          ID_EI         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_AMIGOS(
          ID_EA         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_MASCOTAS(
          ID_EM         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_DEPORTIVOS(
          ID_ED         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);

CREATE TABLE EVENTOS_INDIVIDUALES(
          ID_EIN         VARCHAR2(100) PRIMARY KEY,
          LOCALIDAD     VARCHAR2(100),
          PRECIO        NUMBER(5),
          UBICACION     VARCHAR2(1000),
          OFERTA        VARCHAR2(1000),
          NOMBRE        VARCHAR2(100),
          DURACION      VARCHAR2(10),
          EDAD          VARCHAR2(50),
          OBS           VARCHAR2(1000),
          TIPO          VARCHAR2(100),
          HORA          VARCHAR2(6),
          FECHA         DATE
);
>>>>>>> branch 'master' of https://gitlab.com/yasin_001/Equipo_T04.git
