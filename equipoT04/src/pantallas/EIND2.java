package pantallas;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.JProgressBar;

public class EIND2 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EIND2 frame = new EIND2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EIND2() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Administrador\\Desktop\\T04\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEventosIndividuales = new JLabel("Eventos Individuales");
		lblEventosIndividuales.setFont(new Font("Comic Sans MS", Font.BOLD, 40));
		lblEventosIndividuales.setBounds(227, -6, 414, 70);
		contentPane.add(lblEventosIndividuales);
		
		JButton btnNewButton = new JButton("ESCAPADAS");
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(62, 62, 350, 45);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("EXCURSIONES Y RUTAS");
		btnNewButton_1.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(435, 62, 350, 45);
		contentPane.add(btnNewButton_1);
		
		JButton btnParqueDeAtracciones = new JButton("PARQUE DE ATRACCIONES");
		btnParqueDeAtracciones.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnParqueDeAtracciones.setBounds(62, 551, 350, 45);
		contentPane.add(btnParqueDeAtracciones);
		
		JButton btnDeportes = new JButton("DEPORTES");
		btnDeportes.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnDeportes.setBounds(435, 551, 350, 45);
		contentPane.add(btnDeportes);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EI\\IMG_0503 (4).PNG"));
		lblNewLabel.setBounds(62, 80, 350, 250);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EI\\IMG_0520 (2).PNG"));
		lblNewLabel_2.setBounds(435, 89, 350, 232);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EI\\pamigos (3).png"));
		lblNewLabel_1.setBounds(62, 346, 350, 250);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EI\\IMG_0504 (3).PNG"));
		lblNewLabel_3.setBounds(435, 346, 350, 250);
		contentPane.add(lblNewLabel_3);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(10, 607, 146, 14);
		contentPane.add(progressBar);
	}
}
