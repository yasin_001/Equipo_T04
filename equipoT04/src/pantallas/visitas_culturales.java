package pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import java.awt.Color;
import java.awt.Font;

public class visitas_culturales extends JFrame {

	private JPanel panel_vculturales;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					visitas_culturales frame = new visitas_culturales();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public visitas_culturales() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\Equipo_T04\\APP Eventos\\icono!.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 671);
		panel_vculturales = new JPanel();
		panel_vculturales.setBackground(Color.PINK);
		panel_vculturales.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel_vculturales);
		panel_vculturales.setLayout(null);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(0, 619, 146, 14);
		panel_vculturales.add(progressBar);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(838, 0, 17, 633);
		panel_vculturales.add(scrollBar);
		
		JButton boton1 = new JButton("Museos");
		boton1.setForeground(new Color(128, 0, 128));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton1.setBounds(0, 0, 146, 35);
		panel_vculturales.add(boton1);
		
		JButton boton2 = new JButton("Galer\u00EDas de Arte");
		boton2.setForeground(new Color(128, 0, 128));
		boton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton2.setBounds(410, 0, 146, 35);
		panel_vculturales.add(boton2);
		
		JButton boton3 = new JButton("Exposiciones");
		boton3.setForeground(new Color(128, 0, 128));
		boton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton3.setBounds(501, 598, 146, 35);
		panel_vculturales.add(boton3);
		
		JLabel foto1 = new JLabel("new label");
		foto1.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\museo.png"));
		foto1.setBounds(0, 34, 412, 305);
		panel_vculturales.add(foto1);
		
		JLabel foto2 = new JLabel("New label");
		foto2.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\galeriarte.png"));
		foto2.setBounds(397, 34, 448, 305);
		panel_vculturales.add(foto2);
		
		JLabel foto3 = new JLabel("New label");
		foto3.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\exposiciones.png"));
		foto3.setBounds(0, 339, 584, 294);
		panel_vculturales.add(foto3);
		
		JButton boton4 = new JButton("Ofertas");
		boton4.setForeground(new Color(128, 0, 128));
		boton4.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		boton4.setBounds(639, 402, 171, 144);
		panel_vculturales.add(boton4);
	}
}
