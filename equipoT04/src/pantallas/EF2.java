package pantallas;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JProgressBar;

public class EF2 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EF2 frame = new EF2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EF2() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Administrador\\Desktop\\T04\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 868, 669);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Escapadas");
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton.setBounds(36, 285, 351, 56);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Excursiones y rutas");
		btnNewButton_1.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton_1.setBounds(441, 285, 344, 56);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Deportes y actividades");
		btnNewButton_2.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton_2.setBounds(36, 552, 351, 54);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Parques de atracciones y aquaticos");
		btnNewButton_3.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton_3.setBounds(434, 552, 351, 51);
		contentPane.add(btnNewButton_3);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EF\\IMG_0507 (2).PNG"));
		lblNewLabel.setBounds(36, 93, 351, 224);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EF\\IMG_0508 (2).PNG"));
		lblNewLabel_1.setBounds(441, 93, 344, 224);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EF\\IMG_0506 (2).PNG"));
		lblNewLabel_2.setBounds(36, 366, 351, 224);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EF\\IMG_0477 (2).PNG"));
		lblNewLabel_3.setBounds(434, 361, 351, 235);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Eventos Familiares");
		lblNewLabel_4.setFont(new Font("Comic Sans MS", Font.BOLD, 40));
		lblNewLabel_4.setBounds(211, 0, 502, 82);
		contentPane.add(lblNewLabel_4);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(0, 617, 146, 14);
		contentPane.add(progressBar);
	}
}
