package pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class Eventos_Parejas extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eventos_Parejas frame = new Eventos_Parejas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eventos_Parejas() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Usuario\\Desktop\\T04\\img\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 869, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEventosPareja = new JLabel("EVENTOS PAREJA");
		lblEventosPareja.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
		lblEventosPareja.setBounds(273, 46, 279, 43);
		contentPane.add(lblEventosPareja);
		
		JButton btnEscapadas = new JButton("ESCAPADAS");
		btnEscapadas.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnEscapadas.setBounds(573, 304, 176, 27);
		contentPane.add(btnEscapadas);
		
		JButton btnParqueAtracciones = new JButton("PARQUE ATRACCIONES Y PARQUE AQUATICOS");
		btnParqueAtracciones.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnParqueAtracciones.setBounds(403, 579, 367, 27);
		contentPane.add(btnParqueAtracciones);
		
		JButton btnRelax = new JButton("RELAX");
		btnRelax.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnRelax.setBounds(228, 304, 127, 27);
		contentPane.add(btnRelax);
		
		JButton btnRutasExcurcionesY = new JButton("RUTAS, EXCURSIONES Y DEPORTE");
		btnRutasExcurcionesY.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnRutasExcurcionesY.setBounds(47, 579, 300, 27);
		contentPane.add(btnRutasExcurcionesY);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Usuario\\Downloads\\Foiy63\\photos\\IMG_0514.PNG"));
		lblNewLabel.setBounds(23, 100, 350, 250);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Usuario\\Downloads\\K43F3O\\photos\\IMG_0513.PNG"));
		lblNewLabel_1.setBounds(410, 100, 350, 250);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Usuario\\Downloads\\qHln1u\\photos\\IMG_0515.PNG"));
		lblNewLabel_2.setBounds(23, 371, 350, 250);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\Usuario\\Downloads\\cliY9e\\photos\\pamigos.png"));
		lblNewLabel_3.setBounds(410, 371, 350, 250);
		contentPane.add(lblNewLabel_3);
	}

}
