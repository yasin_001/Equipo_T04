package pantallas;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JMenuBar;
import java.awt.Color;
import javax.swing.JProgressBar;
import javax.swing.JPasswordField;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import equipoT04.Eventos;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Inicio extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio frame = new Inicio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Inicio() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Usuario\\Desktop\\IMG APP\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Usuario\\Desktop\\icon app_t.png"));
		lblNewLabel.setBounds(74, 11, 105, 112);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Usuario\\Desktop\\Sin t\u00EDtulo.jpg"));
		lblNewLabel_1.setBounds(260, 19, 335, 97);
		contentPane.add(lblNewLabel_1);
		
		JButton btnEntrar = new JButton("ENTRAR");
		btnEntrar.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnEntrar.setBounds(674, 43, 150, 23);
		contentPane.add(btnEntrar);
		
		JButton btnRegistrarse = new JButton("REGISTRARSE");
		btnRegistrarse.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnRegistrarse.setBounds(674, 77, 150, 23);
		contentPane.add(btnRegistrarse);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(51, 204, 51));
		panel.setBounds(10, 134, 814, 45);
		contentPane.add(panel);
		
		JButton btncine = new JButton("CINE");
		panel.add(btncine);
		btncine.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btnteatro = new JButton("TEATRO");
		panel.add(btnteatro);
		btnteatro.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btndeportes = new JButton("DEPORTES");
		panel.add(btndeportes);
		btndeportes.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btnrurales = new JButton("ACTIVIDADES RURALES");
		panel.add(btnrurales);
		btnrurales.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btnconciertos = new JButton("CONCIERTOS");
		panel.add(btnconciertos);
		btnconciertos.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JButton btneventos = new JButton("EVENTOS");
		btneventos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Eventos eventos =new Eventos();
				eventos.setVisible(true);
			}
		});
		panel.add(btneventos);
		btneventos.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Usuario\\Desktop\\Sin t\u00EDtulos.jpg"));
		lblNewLabel_2.setBounds(27, 203, 777, 372);
		contentPane.add(lblNewLabel_2);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(10, 607, 146, 14);
		contentPane.add(progressBar);
		this.setSize(870, 670);
		
		ImageIcon fot = new ImageIcon();
		Component jLabel1;
		//Icon icono = new ImageIcon(fot.getImage().getScaledInstance(jLabel1.getWidth(), jLabel1.getHeight(), Image.SCALE_DEFAULT));
		//jLabel1.setIcon(icono);
		this.repaint();
	}
}
