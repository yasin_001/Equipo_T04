package pantallas;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;

public class e_jubilados2 extends JFrame {

	private JPanel panel_ejubilados;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					e_jubilados2 frame = new e_jubilados2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public e_jubilados2() {
		
		//Ventanas
		
		setTitle("Eventos para Jubilados");
		setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\Equipo_T04\\APP Eventos\\icono!.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 671);
		
		//Panel
		panel_ejubilados = new JPanel();
		panel_ejubilados.setBackground(new Color(32, 178, 170));
		panel_ejubilados.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel_ejubilados);
		panel_ejubilados.setLayout(null);
		
		//Barras
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(838, 0, 17, 633);
		panel_ejubilados.add(scrollBar);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(688, 619, 146, 14);
		panel_ejubilados.add(progressBar);
		
		
		//Botones
		JButton boton1 = new JButton("Escapadas");
		boton1.setForeground(new Color(0, 128, 128));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton1.setBounds(0, 0, 196, 31);
		panel_ejubilados.add(boton1);
		
		JButton boton2 = new JButton("Torneos deportivos y actividades");
		boton2.setForeground(new Color(0, 128, 128));
		boton2.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton2.setBounds(433, 258, 235, 55);
		panel_ejubilados.add(boton2);
		
		JButton boton3 = new JButton("Excursiones y rutas");
		boton3.setForeground(new Color(0, 128, 128));
		boton3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton3.setBounds(0, 549, 186, 45);
		panel_ejubilados.add(boton3);
		
		JButton boton4 = new JButton("Spa y Relax");
		boton4.setForeground(new Color(0, 128, 128));
		boton4.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton4.setBounds(397, 593, 235, 29);
		panel_ejubilados.add(boton4);
		
		JButton boton5 = new JButton("Ofertas");
		boton5.setForeground(new Color(0, 128, 128));
		boton5.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton5.setBounds(126, 593, 126, 40);
		panel_ejubilados.add(boton5);
		
		
		//Fotos (etiquetas)
		JLabel foto1 = new JLabel("New label");
		foto1.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\IMG_0510.PNG"));
		foto1.setBounds(0, 0, 485, 262);
		panel_ejubilados.add(foto1);
		
		JLabel foto3 = new JLabel("New label");
		foto3.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\IMG_0511.PNG"));
		foto3.setBounds(433, 0, 412, 313);
		panel_ejubilados.add(foto3);
		
		JLabel foto2 = new JLabel("New label");
		foto2.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\IMG_0509.PNG"));
		foto2.setBounds(0, 258, 530, 336);
		panel_ejubilados.add(foto2);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\IMG_0512.PNG"));
		lblNewLabel.setBounds(399, 303, 446, 319);
		panel_ejubilados.add(lblNewLabel);
		
		JButton btnOtros = new JButton("Otros");
		btnOtros.setForeground(new Color(0, 128, 128));
		btnOtros.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		btnOtros.setBounds(253, 593, 100, 40);
		panel_ejubilados.add(btnOtros);
		
		//Etiqueta
		JLabel lblYAdems = new JLabel("Y adem\u00E1s...");
		lblYAdems.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		lblYAdems.setBounds(10, 593, 119, 40);
		panel_ejubilados.add(lblYAdems);
		
		
	}

}
