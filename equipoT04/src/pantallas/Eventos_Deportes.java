package pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Eventos_Deportes extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eventos_Deportes frame = new Eventos_Deportes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eventos_Deportes() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Usuario\\Desktop\\T04\\img\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEventosDeportes = new JLabel("EVENTOS DEPORTES");
		lblEventosDeportes.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
		lblEventosDeportes.setBounds(266, 39, 322, 43);
		contentPane.add(lblEventosDeportes);
		
		JButton btnTorneos = new JButton("TORNEOS");
		btnTorneos.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnTorneos.setBounds(10, 157, 165, 23);
		contentPane.add(btnTorneos);
		
		JButton btnRutas = new JButton("RUTAS");
		btnRutas.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnRutas.setBounds(10, 191, 165, 23);
		contentPane.add(btnRutas);
		
		JButton btnMaratones = new JButton("MARATONES");
		btnMaratones.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnMaratones.setBounds(10, 225, 165, 23);
		contentPane.add(btnMaratones);
		
		JButton btnKarting = new JButton("KARTING");
		btnKarting.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnKarting.setBounds(10, 259, 165, 23);
		contentPane.add(btnKarting);
		
		JButton btnNewButton = new JButton("FUTBOL BOLAS");
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnNewButton.setBounds(10, 293, 165, 23);
		contentPane.add(btnNewButton);
		
		JButton btnCajaEnigma = new JButton("CAJA ENIGMA");
		btnCajaEnigma.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnCajaEnigma.setBounds(10, 327, 165, 23);
		contentPane.add(btnCajaEnigma);
		
		JButton btnNewButton_1 = new JButton("PAINT BALL");
		btnNewButton_1.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnNewButton_1.setBounds(10, 361, 146, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnDeportesAquaticos = new JButton("DEPORTES AQUATICOS");
		btnDeportesAquaticos.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnDeportesAquaticos.setBounds(10, 395, 226, 23);
		contentPane.add(btnDeportesAquaticos);
		
		JButton btnDeportesAereos = new JButton("DEPORTES AEREOS");
		btnDeportesAereos.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnDeportesAereos.setBounds(10, 497, 195, 23);
		contentPane.add(btnDeportesAereos);
		
		JButton btnDeportesTerrestres = new JButton("DEPORTES TERRESTRES");
		btnDeportesTerrestres.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnDeportesTerrestres.setBounds(10, 463, 233, 23);
		contentPane.add(btnDeportesTerrestres);
		
		JButton btnDeportesExtremos = new JButton("DEPORTES EXTREMOS");
		btnDeportesExtremos.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		btnDeportesExtremos.setBounds(10, 429, 211, 23);
		contentPane.add(btnDeportesExtremos);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Usuario\\Desktop\\T04\\img\\ED\\IMG_0522.PNG"));
		lblNewLabel.setBounds(224, 87, 620, 534);
		contentPane.add(lblNewLabel);
	}

}
