package pantallas;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EM2 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EM2 frame = new EM2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EM2() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Administrador\\Desktop\\T04\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 671);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEventosMascotas = new JLabel("Eventos Mascotas");
		lblEventosMascotas.setFont(new Font("Comic Sans MS", Font.BOLD, 40));
		lblEventosMascotas.setBounds(238, -18, 370, 104);
		contentPane.add(lblEventosMascotas);
		
		JButton btnEscapadas = new JButton("Escapadas");
		btnEscapadas.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnEscapadas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEscapadas.setBounds(56, 276, 349, 48);
		contentPane.add(btnEscapadas);
		
		JButton btnNewButton = new JButton("Concursos");
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(438, 276, 349, 46);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Encuentros");
		btnNewButton_1.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(56, 564, 349, 48);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Rutas");
		btnNewButton_2.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		btnNewButton_2.setBounds(438, 564, 349, 48);
		contentPane.add(btnNewButton_2);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EM\\IMG_0516 (2).PNG"));
		lblNewLabel.setBounds(56, 72, 349, 250);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EM\\IMG_0517 (2).PNG"));
		lblNewLabel_1.setBounds(438, 72, 349, 250);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EM\\IMG_0519 (2).PNG"));
		lblNewLabel_2.setBounds(56, 359, 349, 228);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EM\\IMG_0518 (2).PNG"));
		lblNewLabel_3.setBounds(438, 359, 349, 228);
		contentPane.add(lblNewLabel_3);
	}

}
