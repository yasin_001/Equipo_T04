package pantallas;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class EA2 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EA2 frame = new EA2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EA2() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Administrador\\Desktop\\T04\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 868, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Escapadas, rutas y excursiones");
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton.setBounds(29, 297, 350, 48);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Deportes y Talleres");
		btnNewButton_1.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton_1.setBounds(428, 297, 339, 48);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Torneos");
		btnNewButton_2.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton_2.setBounds(29, 560, 350, 47);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Parques de atracciones y parques aquaticos");
		btnNewButton_3.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		btnNewButton_3.setBounds(427, 560, 352, 46);
		contentPane.add(btnNewButton_3);
		
		JLabel lblEventosAmigos = new JLabel("Eventos Amigos");
		lblEventosAmigos.setFont(new Font("Comic Sans MS", Font.BOLD, 40));
		lblEventosAmigos.setBounds(254, 0, 339, 72);
		contentPane.add(lblEventosAmigos);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EA\\escaamigos (2).png"));
		lblNewLabel.setBounds(29, 83, 350, 241);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EIND\\IMG_0504 (3).PNG"));
		lblNewLabel_1.setBounds(428, 83, 339, 241);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EA\\tamigos (2).png"));
		lblNewLabel_2.setBounds(29, 356, 350, 241);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EIND\\pamigos (3).png"));
		lblNewLabel_3.setBounds(429, 356, 350, 241);
		contentPane.add(lblNewLabel_3);
	}

}
