package pantallas;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class EI2 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EI2 frame = new EI2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EI2() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Administrador\\Desktop\\T04\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 868, 669);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Colonias y Campamentos");
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton.setBounds(43, 284, 336, 41);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Talleres y Actividades");
		btnNewButton_1.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton_1.setBounds(434, 284, 347, 41);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Deportes");
		btnNewButton_2.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton_2.setBounds(43, 567, 336, 41);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Rutas y Excursiones");
		btnNewButton_3.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton_3.setBounds(434, 567, 347, 41);
		contentPane.add(btnNewButton_3);
		
		JLabel lblEventosInfantiles = new JLabel("Eventos Infantiles");
		lblEventosInfantiles.setFont(new Font("Comic Sans MS", Font.BOLD, 40));
		lblEventosInfantiles.setBounds(246, 0, 360, 65);
		contentPane.add(lblEventosInfantiles);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EI\\camping (2).jpg"));
		lblNewLabel.setBounds(43, 73, 336, 243);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EI\\talleres-infantiles (2).jpg"));
		lblNewLabel_1.setBounds(434, 76, 347, 237);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EI\\Deporte-para-ninos_reference (2).jpg"));
		lblNewLabel_2.setBounds(43, 347, 336, 237);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\Administrador\\Desktop\\T04\\EF\\IMG_0508 (2).PNG"));
		lblNewLabel_3.setBounds(434, 341, 347, 243);
		contentPane.add(lblNewLabel_3);
	}

}
