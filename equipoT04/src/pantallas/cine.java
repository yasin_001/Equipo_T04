package src.pantallas;
import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.border.MatteBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;

public class cine extends JFrame {

	private JPanel panel_cine;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cine frame = new cine();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public cine() {
		
		//Ventana
		setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\Equipo_T04\\APP Eventos\\icono!.png"));
		setTitle("Cine");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 878, 674);
		
		//Panel
		panel_cine = new JPanel();
		panel_cine.setBackground(Color.WHITE);
		panel_cine.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel_cine);
		panel_cine.setLayout(null);
		
		
		//Botones
		JButton boton1 = new JButton("Cartelera");
		boton1.setBackground(new Color(255, 69, 0));
		boton1.setForeground(new Color(255, 255, 255));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton1.setBounds(10, 15, 112, 43);
		panel_cine.add(boton1);
		
		JButton boton2 = new JButton("Estrenos");
		boton2.setBackground(new Color(255, 69, 0));
		boton2.setForeground(new Color(255, 255, 255));
		boton2.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton2.setBounds(132, 15, 105, 43);
		panel_cine.add(boton2);
		
		JButton boton3 = new JButton("Tr\u00E1ilers");
		boton3.setBackground(new Color(255, 69, 0));
		boton3.setForeground(new Color(255, 255, 255));
		boton3.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton3.setBounds(247, 15, 112, 43);
		panel_cine.add(boton3);
		
		JButton boton4 = new JButton("Pr\u00F3ximos estrenos");
		boton4.setBackground(new Color(255, 69, 0));
		boton4.setForeground(new Color(255, 255, 255));
		boton4.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton4.setBounds(369, 15, 170, 43);
		panel_cine.add(boton4);
		
		JButton boton5 = new JButton("Pr\u00F3ximamente en cine");
		boton5.setBackground(new Color(255, 69, 0));
		boton5.setForeground(new Color(255, 255, 255));
		boton5.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton5.setBounds(549, 15, 176, 42);
		panel_cine.add(boton5);
		
		JButton boton6 = new JButton("OFERTAS");
		boton6.setBackground(new Color(255, 69, 0));
		boton6.setForeground(new Color(255, 255, 255));
		boton6.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton6.setBounds(735, 15, 105, 43);
		panel_cine.add(boton6);
		
		
		//Foto fondo (etiqueta)
		JLabel foto1 = new JLabel("");
		foto1.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\cine.PNG"));
		foto1.setBounds(24, 69, 784, 551);
		panel_cine.add(foto1);
		
		
		//Barras
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(845, 0, 17, 636);
		panel_cine.add(scrollBar);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(10, 622, 146, 14);
		panel_cine.add(progressBar);
		
	}
}
