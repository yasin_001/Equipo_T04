package pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Deportes extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Deportes frame = new Deportes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Deportes() {
		setTitle("Deportes");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Usuario\\Desktop\\icon app_t.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 670);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnFutbol = new JButton("FUTBOL");
		btnFutbol.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnFutbol.setBounds(10, 11, 171, 31);
		contentPane.add(btnFutbol);
		
		JButton btnRugby = new JButton("RUGBY");
		btnRugby.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnRugby.setBounds(191, 53, 171, 31);
		contentPane.add(btnRugby);
		
		JButton btnNewButton = new JButton("TENNIS");
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnNewButton.setBounds(553, 11, 171, 31);
		contentPane.add(btnNewButton);
		
		JButton btnBmx = new JButton("BMX");
		btnBmx.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnBmx.setBounds(191, 11, 171, 31);
		contentPane.add(btnBmx);
		
		JButton btnBasquet = new JButton("BASQUET");
		btnBasquet.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnBasquet.setBounds(10, 53, 171, 31);
		contentPane.add(btnBasquet);
		
		JButton btnGolf = new JButton("GOLF");
		btnGolf.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnGolf.setBounds(372, 11, 171, 31);
		contentPane.add(btnGolf);
		
		JButton btnOtrosDeportes = new JButton("OTROS");
		btnOtrosDeportes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnOtrosDeportes.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnOtrosDeportes.setBounds(734, 11, 96, 73);
		contentPane.add(btnOtrosDeportes);
		
		JButton btnMotos = new JButton("MOTOS");
		btnMotos.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnMotos.setBounds(372, 53, 171, 31);
		contentPane.add(btnMotos);
		
		JButton btnCoches = new JButton("COCHES");
		btnCoches.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		btnCoches.setBounds(553, 53, 171, 31);
		contentPane.add(btnCoches);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("https://gitlab.com/yasin_001/Equipo_T04/blob/e5c38cab1fd43b8ba10adf5f38a8e6a30bff7847/equipoT04/src/IMAGENES/tenis.png"));
		lblNewLabel_2.setBounds(0, 0, 870, 670);
		contentPane.add(lblNewLabel_2);
	}
}
