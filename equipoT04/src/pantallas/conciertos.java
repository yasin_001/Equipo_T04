package pantallas;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class conciertos extends JFrame {

	private JPanel panel_conciertos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					conciertos frame = new conciertos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public conciertos() {
		
		//Ventana
		setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\Equipo_T04\\APP Eventos\\icono!.png"));
		setTitle("Conciertos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 670);
		
		//Panel
		panel_conciertos = new JPanel();
		panel_conciertos.setBackground(new Color(255, 255, 255));
		panel_conciertos.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel_conciertos);
		panel_conciertos.setLayout(null);
		
		
		//Barra
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(828, 0, 17, 632);
		panel_conciertos.add(scrollBar);
		
		
		//Botones
		JButton boton1 = new JButton("Ambient");
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton1.setForeground(new Color(255, 0, 255));
		boton1.setBackground(new Color(255, 255, 0));
		boton1.setBounds(140, 162, 108, 61);
		panel_conciertos.add(boton1);
		
		JButton boton2 = new JButton("Barroca/Cl\u00E1sica");
		boton2.setForeground(new Color(255, 0, 255));
		boton2.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton2.setBackground(new Color(255, 255, 0));
		boton2.setBounds(258, 162, 142, 61);
		panel_conciertos.add(boton2);
		
		JButton boton3 = new JButton("Rock Cl\u00E1sico");
		boton3.setForeground(new Color(255, 0, 255));
		boton3.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton3.setBackground(new Color(255, 255, 0));
		boton3.setBounds(410, 162, 125, 61);
		panel_conciertos.add(boton3);
		
		JButton boton4 = new JButton("Country");
		boton4.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton4.setForeground(new Color(255, 0, 255));
		boton4.setBackground(new Color(255, 255, 0));
		boton4.setBounds(545, 162, 116, 61);
		panel_conciertos.add(boton4);
		
		JButton boton5 = new JButton("Dubstep");
		boton5.setForeground(new Color(255, 0, 255));
		boton5.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton5.setBackground(new Color(255, 255, 0));
		boton5.setBounds(671, 162, 108, 61);
		panel_conciertos.add(boton5);
		
		JButton boton6 = new JButton("Heavy Metal");
		boton6.setForeground(new Color(255, 0, 255));
		boton6.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton6.setBackground(new Color(255, 255, 0));
		boton6.setBounds(10, 367, 99, 58);
		panel_conciertos.add(boton6);
		
		JButton boton7 = new JButton("Indie Rock");
		boton7.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton7.setForeground(new Color(255, 0, 255));
		boton7.setBackground(new Color(255, 255, 0));
		boton7.setBounds(131, 367, 116, 58);
		panel_conciertos.add(boton7);
		
		JButton boton8 = new JButton("Jazz");
		boton8.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton8.setForeground(new Color(255, 0, 255));
		boton8.setBackground(new Color(255, 255, 0));
		boton8.setBounds(270, 367, 103, 58);
		panel_conciertos.add(boton8);
		
		JButton boton9 = new JButton("Oldies");
		boton9.setForeground(new Color(255, 0, 255));
		boton9.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton9.setBackground(new Color(255, 255, 0));
		boton9.setBounds(407, 367, 106, 58);
		panel_conciertos.add(boton9);
		
		JButton boton10 = new JButton("Polka");
		boton10.setForeground(new Color(255, 0, 255));
		boton10.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton10.setBackground(new Color(255, 255, 0));
		boton10.setBounds(545, 367, 103, 58);
		panel_conciertos.add(boton10);
		
		JButton boton11 = new JButton("Pop");
		boton11.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton11.setForeground(new Color(255, 0, 255));
		boton11.setBackground(new Color(255, 255, 0));
		boton11.setBounds(685, 367, 94, 58);
		panel_conciertos.add(boton11);
		
		JButton boton12 = new JButton("Punk");
		boton12.setForeground(new Color(255, 0, 255));
		boton12.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton12.setBackground(new Color(255, 255, 0));
		boton12.setBounds(10, 563, 111, 58);
		panel_conciertos.add(boton12);
		
		JButton boton13 = new JButton("Rap/Hip-Hop");
		boton13.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton13.setForeground(new Color(255, 0, 255));
		boton13.setBackground(new Color(255, 255, 0));
		boton13.setBounds(131, 563, 117, 58);
		panel_conciertos.add(boton13);
		
		JButton boton14 = new JButton("Reggae");
		boton14.setForeground(new Color(255, 0, 255));
		boton14.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton14.setBackground(new Color(255, 255, 0));
		boton14.setBounds(270, 563, 116, 58);
		panel_conciertos.add(boton14);
		
		JButton boton15 = new JButton("Rock");
		boton15.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton15.setForeground(new Color(255, 0, 255));
		boton15.setBackground(new Color(255, 255, 0));
		boton15.setBounds(410, 560, 103, 61);
		panel_conciertos.add(boton15);
		
		JButton boton16 = new JButton("Smooth Jazz");
		boton16.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton16.setForeground(new Color(255, 0, 255));
		boton16.setBackground(new Color(255, 255, 0));
		boton16.setBounds(536, 563, 125, 58);
		panel_conciertos.add(boton16);
		
		JButton boton17 = new JButton("Techno/Trance");
		boton17.setForeground(new Color(255, 0, 255));
		boton17.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton17.setBackground(new Color(255, 255, 0));
		boton17.setBounds(671, 563, 125, 58);
		panel_conciertos.add(boton17);
		
		JButton boton18 = new JButton("OFERTAS");
		boton18.setForeground(new Color(255, 0, 255));
		boton18.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton18.setBackground(new Color(255, 255, 0));
		boton18.setBounds(556, 0, 250, 40);
		panel_conciertos.add(boton18);
		
		JButton boton19 = new JButton("Otros");
		boton19.setForeground(new Color(255, 0, 255));
		boton19.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton19.setBackground(new Color(255, 255, 0));
		boton19.setBounds(0, 93, 137, 97);
		panel_conciertos.add(boton19);
		
		JButton boton20 = new JButton("Otros");
		boton20.setBackground(new Color(255, 255, 0));
		boton20.setForeground(new Color(255, 0, 255));
		boton20.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton20.setBounds(0, 0, 137, 97);
		panel_conciertos.add(boton20);
		
		//Barra
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(10, 618, 210, 14);
		panel_conciertos.add(progressBar);
		
		
		//Foto fondo
		JLabel foto1 = new JLabel("New label");
		foto1.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\conciertos.png"));
		foto1.setBounds(10, 36, 796, 585);
		panel_conciertos.add(foto1);
	}

}
