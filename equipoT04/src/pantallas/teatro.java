package pantallas;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.Font;

public class teatro extends JFrame {

	private JPanel panel_teatro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					teatro frame = new teatro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public teatro() {
		
		//Ventana
		setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\Equipo_T04\\APP Eventos\\icono!.png"));
		setTitle("Teatro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 670);
		
		//Panel
		panel_teatro = new JPanel();
		panel_teatro.setBackground(new Color(255, 0, 0));
		panel_teatro.setForeground(new Color(255, 0, 0));
		panel_teatro.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel_teatro);
		panel_teatro.setLayout(null);
		
		
		//Barras
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(0, 618, 156, 14);
		panel_teatro.add(progressBar);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(838, 0, 17, 632);
		panel_teatro.add(scrollBar);
		
		//Botones
		JButton boton1 = new JButton("Cartelera");
		boton1.setForeground(new Color(250, 250, 210));
		boton1.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		boton1.setBackground(new Color(25, 25, 112));
		boton1.setBounds(41, 0, 129, 44);
		panel_teatro.add(boton1);
		
		JButton boton4 = new JButton("OFERTAS");
		boton4.setBackground(new Color(25, 25, 112));
		boton4.setForeground(new Color(250, 250, 210));
		boton4.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton4.setBounds(622, 4, 176, 41);
		panel_teatro.add(boton4);
		
		JButton boton2 = new JButton("Estrenos");
		boton2.setBackground(new Color(25, 25, 112));
		boton2.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton2.setForeground(new Color(250, 250, 210));
		boton2.setBounds(207, 2, 156, 44);
		panel_teatro.add(boton2);
		
		JButton boton3 = new JButton("Pr\u00F3ximos estrenos");
		boton3.setBackground(new Color(25, 25, 112));
		boton3.setForeground(new Color(250, 250, 210));
		boton3.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		boton3.setBounds(413, 3, 176, 42);
		panel_teatro.add(boton3);
		
		
		//Fondo
		JLabel foto1 = new JLabel("New label");
		foto1.setIcon(new ImageIcon("C:\\Users\\Usuari\\Desktop\\teatro.jpg"));
		foto1.setBounds(29, 44, 799, 588);
		panel_teatro.add(foto1);
	}

}
