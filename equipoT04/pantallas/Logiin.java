package pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import equipoT04.Reserva;
import equipoT04.Reserva1;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Toolkit;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Logiin extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Logiin frame = new Logiin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Logiin() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Usuario\\Desktop\\icon app_t.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 681);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblUsuario.setBounds(45, 259, 92, 14);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblContrasea.setBounds(45, 322, 148, 14);
		contentPane.add(lblContrasea);
		
		textField = new JTextField();
		textField.setBounds(144, 258, 133, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(144, 316, 133, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel label = new JLabel("Usuario:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(530, 261, 92, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Contrase\u00F1a:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_1.setBounds(530, 319, 148, 14);
		contentPane.add(label_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(632, 258, 174, 20);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(632, 316, 174, 20);
		contentPane.add(textField_3);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblEmail.setBounds(530, 366, 92, 14);
		contentPane.add(lblEmail);
		
		JLabel lblolvidasteTuContrasea = new JLabel("\u00BFOlvidaste tu contrase\u00F1a?");
		lblolvidasteTuContrasea.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblolvidasteTuContrasea.setBounds(144, 347, 193, 14);
		contentPane.add(lblolvidasteTuContrasea);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTelefono.setBounds(530, 427, 118, 14);
		contentPane.add(lblTelefono);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(632, 423, 174, 20);
		contentPane.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(632, 365, 174, 20);
		contentPane.add(textField_5);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setFont(new Font("Malgun Gothic", Font.PLAIN, 16));
		btnEntrar.setBounds(138, 402, 92, 31);
		contentPane.add(btnEntrar);
		
		JButton btnRegistrate = new JButton("Registrate");
		btnRegistrate.setFont(new Font("Malgun Gothic", Font.PLAIN, 16));
		btnRegistrate.setBounds(632, 472, 118, 31);
		contentPane.add(btnRegistrate);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Usuario\\Desktop\\123"));
		lblNewLabel_1.setBounds(10, 0, 118, 115);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblYaEresUsuario = new JLabel("YA ERES USUARIO");
		lblYaEresUsuario.setForeground(new Color(0, 0, 255));
		lblYaEresUsuario.setFont(new Font("Segoe UI", Font.BOLD, 18));
		lblYaEresUsuario.setBounds(110, 187, 179, 63);
		contentPane.add(lblYaEresUsuario);
		
		JLabel lblRegistrateEnFun = new JLabel("REGISTRATE EN FUN EVENTS");
		lblRegistrateEnFun.setForeground(new Color(0, 0, 255));
		lblRegistrateEnFun.setFont(new Font("Segoe UI", Font.BOLD, 18));
		lblRegistrateEnFun.setBounds(530, 187, 270, 63);
		contentPane.add(lblRegistrateEnFun);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Usuario\\Downloads\\WxJoxO\\photos\\icon app.png"));
		lblNewLabel.setBounds(20, 26, 150, 150);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Usuario\\Desktop\\APP Eventos\\img\\Sin t\u00EDtulo.png"));
		lblNewLabel_2.setBounds(217, 41, 518, 135);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("Seguir con la compra");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Reserva reserva_usu = new Reserva();
				reserva_usu.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnNewButton.setBounds(110, 456, 174, 44);
		contentPane.add(btnNewButton);
		
		JButton btnRegistrateYSigue = new JButton("Registrate y sigue con la compra");
		btnRegistrateYSigue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Reserva1 reserva1 = new Reserva1();
				reserva1.setVisible(true);
			}
		});
		btnRegistrateYSigue.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		btnRegistrateYSigue.setBounds(548, 514, 252, 53);
		contentPane.add(btnRegistrateYSigue);
	}
}
