package equipoT04;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Eventos extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eventos frame = new Eventos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eventos() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Administrador\\Desktop\\T04\\icon app.png"));
		setTitle("EVENTOS");
		setForeground(new Color(0, 0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 871, 671);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 255, 255));
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEventosAmigos = new JLabel("EVENTOS AMIGOS");
		lblEventosAmigos.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblEventosAmigos.setBounds(10, 11, 144, 14);
		contentPane.add(lblEventosAmigos);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("TORNEO FUTBOL7. Inscripcion gratuita.Torneo Futbol7 en poliderportivo municipal de Valls dia 26/06/16.Para mayores de 18 a\u00F1os.Mas info:977586533");
		rdbtnNewRadioButton.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton.setBounds(10, 109, 800, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("RUTA SENDERISMO AMIGOS. Inscripci\u00F3n gratuita. Ruta lila por Horta de San Joan para grupo de amigos.Ruta para el dia 26/06/16.Mas info:977586524");
		rdbtnNewRadioButton_1.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_1.setBounds(10, 32, 800, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("TALLER DE CASTELLS. Inscripci\u00F3n gratuita.Taller de Castells al Serrallo dia 26/06/16.Taller para grupo de amigos .Mas info:977586525");
		rdbtnNewRadioButton_2.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_2.setBounds(10, 58, 800, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		JLabel lblEventosDeportivos = new JLabel("EVENTOS DEPORTIVOS");
		lblEventosDeportivos.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblEventosDeportivos.setBounds(10, 88, 185, 14);
		contentPane.add(lblEventosDeportivos);
		
		JLabel lblEventosFamiliares = new JLabel("EVENTOS FAMILIARES");
		lblEventosFamiliares.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblEventosFamiliares.setBounds(10, 139, 167, 14);
		contentPane.add(lblEventosFamiliares);
		
		JRadioButton rdbtnNewRadioButton_6 = new JRadioButton("RUTA SENDERISMO FAMILIAR. Inscripci\u00F3n gratuita.Ruta verde por Horta de San Joan para familias.Ruta para el dia 26/06/16.Mas info:977586524");
		rdbtnNewRadioButton_6.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_6.setBounds(10, 160, 800, 23);
		contentPane.add(rdbtnNewRadioButton_6);
		
		JRadioButton rdbtnNewRadioButton_7 = new JRadioButton("RUTA SENDERISMO. Inscripci\u00F3n gratuita.Ruta roja por Horta de San Joan. Ruta para el dia 26/06/16.Ruta para mayores de 18 a\u00F1os.Mas info:977586524");
		rdbtnNewRadioButton_7.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_7.setBounds(10, 211, 800, 23);
		contentPane.add(rdbtnNewRadioButton_7);
		
		JRadioButton rdbtnNewRadioButton_8 = new JRadioButton("BICICLETADA INFANTIL. Inscripci\u00F3n gratuita.Biciletada para ni\u00F1os de 5 a 12a\u00F1os en mercado de Bonavista a las 10:00h dia 25/06/16.Mas info:977586545");
		rdbtnNewRadioButton_8.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_8.setBounds(10, 262, 800, 23);
		contentPane.add(rdbtnNewRadioButton_8);
		
		JLabel lblEventosIndividuales = new JLabel("EVENTOS INDIVIDUALES");
		lblEventosIndividuales.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblEventosIndividuales.setBounds(10, 190, 202, 14);
		contentPane.add(lblEventosIndividuales);
		
		JRadioButton rdbtnNewRadioButton_9 = new JRadioButton("TALLER DE MARIONETAS. Inscripci\u00F3n gratuita.Taller para ni\u00F1os de 5 a 12a\u00F1os en centro civico de Bonavista a las 11h dia 26/06/16.Mas info:977586523");
		rdbtnNewRadioButton_9.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_9.setBounds(10, 288, 800, 23);
		contentPane.add(rdbtnNewRadioButton_9);
		
		JRadioButton rdbtnNewRadioButton_10 = new JRadioButton("RUTA SENDERISMO JUBILADOS. Inscripci\u00F3n gratuita. Ruta blanca por Horta de San Joan. Ruta para el dia 26/06/16.Mas info:977586524");
		rdbtnNewRadioButton_10.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_10.setBounds(10, 339, 800, 23);
		contentPane.add(rdbtnNewRadioButton_10);
		
		JRadioButton rdbtnNewRadioButton_11 = new JRadioButton("RUTA SENDERISMO CON MASCOTAS. Inscripci\u00F3n gratuita. Ruta azul por Horta de San Joan con mascotas.Ruta para el dia 26/06/16.Mas info:977586524");
		rdbtnNewRadioButton_11.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_11.setBounds(10, 416, 800, 23);
		contentPane.add(rdbtnNewRadioButton_11);
		
		JLabel lblNewLabel = new JLabel("EVENTOS INFANTILES");
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblNewLabel.setBounds(10, 241, 185, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblEventosJubilados = new JLabel("EVENTOS JUBILADOS");
		lblEventosJubilados.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblEventosJubilados.setBounds(10, 318, 167, 14);
		contentPane.add(lblEventosJubilados);
		
		JLabel lblNewLabel_1 = new JLabel("EVENTOS MASCOTAS");
		lblNewLabel_1.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblNewLabel_1.setBounds(10, 395, 167, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("EVENTOS PAREJAS");
		lblNewLabel_2.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblNewLabel_2.setBounds(10, 451, 167, 14);
		contentPane.add(lblNewLabel_2);
		
		JRadioButton rdbtnNewRadioButton_3 = new JRadioButton("RUTA SENDERISMO PAREJAS. Inscripci\u00F3n gratuita. Ruta amarilla por Horta de San Joan para parejas.Ruta para el dia 26/06/16.Mas info:977586524");
		rdbtnNewRadioButton_3.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_3.setBounds(6, 472, 804, 23);
		contentPane.add(rdbtnNewRadioButton_3);
		
		JRadioButton rdbtnNewRadioButton_4 = new JRadioButton("AQUAGYM. Inscripci\u00F3n gratuita. Aquagym para jubilados durante toda la ma\u00F1ana del 26/06/16 en distintas sesiones.Mas info:977586536");
		rdbtnNewRadioButton_4.setFont(new Font("Comic Sans MS", Font.BOLD, 10));
		rdbtnNewRadioButton_4.setBounds(10, 365, 800, 23);
		contentPane.add(rdbtnNewRadioButton_4);
		
		JLabel lblProximosEventos = new JLabel("PROXIMOS EVENTOS");
		lblProximosEventos.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		lblProximosEventos.setForeground(new Color(0, 0, 0));
		lblProximosEventos.setBounds(10, 502, 227, 14);
		contentPane.add(lblProximosEventos);
		
		JLabel lblEscapaRomanticaA = new JLabel("ESCAPA ROMANTICA A PARIS ");
		lblEscapaRomanticaA.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblEscapaRomanticaA.setBounds(10, 527, 202, 14);
		contentPane.add(lblEscapaRomanticaA);
		
		JLabel lblSparelaxParaJubilados = new JLabel("SPA-RELAX PARA JUBILADOS");
		lblSparelaxParaJubilados.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblSparelaxParaJubilados.setBounds(10, 554, 202, 14);
		contentPane.add(lblSparelaxParaJubilados);
		
		JLabel lblParquesAtraccionesY = new JLabel("PARQUES ATRACCIONES Y ACUATICOS PARA TODOS");
		lblParquesAtraccionesY.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblParquesAtraccionesY.setBounds(265, 528, 418, 14);
		contentPane.add(lblParquesAtraccionesY);
		
		JLabel lblNewLabel_3 = new JLabel("COLONIAS INFANTILES");
		lblNewLabel_3.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblNewLabel_3.setBounds(10, 579, 167, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblCampamentosParaJovenes = new JLabel("CAMPAMENTOS PARA JOVENES DE 14 A 16 A\u00D1OS");
		lblCampamentosParaJovenes.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblCampamentosParaJovenes.setBounds(265, 554, 355, 14);
		contentPane.add(lblCampamentosParaJovenes);
		
		JLabel lblEscapadaRuralFamiliar = new JLabel("ESCAPADA RURAL FAMILIAR ");
		lblEscapadaRuralFamiliar.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblEscapadaRuralFamiliar.setBounds(265, 579, 202, 14);
		contentPane.add(lblEscapadaRuralFamiliar);
		
		JButton btnReservar = new JButton("Reservar");
		btnReservar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Reservas frame = new Reservas();
				frame.setVisible(true);
			}
		});
		btnReservar.setBounds(651, 527, 131, 66);
		contentPane.add(btnReservar);
	}
}
