package equipoT04;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class sqlEventos {
	
	private static java.sql.Connection Conexion = null;
	
	//MÉTODO PERSONALIZADO DE CONEXION A SQL
		public void sqlEventosConnection(String user, String pass, String db_name) throws Exception {
		        try {
		            Class.forName("oracle.jdbc.driver.OracleDriver");
		            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
		            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
		        } catch (ClassNotFoundException ex) {
		            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
		        }
		}
		//MÉTODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexión con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexión con el servidor");
	
		        }
		}
}