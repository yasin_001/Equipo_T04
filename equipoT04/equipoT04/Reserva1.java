package equipoT04;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import WIP_Raquel.Inicio;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Reserva1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Reserva1 frame = new Reserva1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Reserva1() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Administrador\\Desktop\\T04\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 618, 156);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ya esta registrado.");
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblNewLabel.setBounds(10, 11, 459, 24);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Sus datos y su/s entrada/s o inscripion/es realizadas ya estan reservadas y enviadas a su email.");
		lblNewLabel_1.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
		lblNewLabel_1.setBounds(10, 46, 582, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblGraciasPorUsar = new JLabel("Gracias por usar nuestra app.");
		lblGraciasPorUsar.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		lblGraciasPorUsar.setBounds(10, 82, 161, 14);
		contentPane.add(lblGraciasPorUsar);
		
		JButton btnVolverAInicio = new JButton("VOLVER A INICIO");
		btnVolverAInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inicio inicio = new Inicio();
				inicio.setVisible(true);
			}
		});
		btnVolverAInicio.setBounds(262, 79, 207, 23);
		contentPane.add(btnVolverAInicio);
	}

}
