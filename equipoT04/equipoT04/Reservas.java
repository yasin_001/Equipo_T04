package equipoT04;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import pantallas.Logiin;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Reservas extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Reservas frame = new Reservas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Reservas() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Administrador\\Desktop\\T04\\icon app.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 241, 331);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("N\u00FAmero entradas o inscripciones");
		lblNewLabel.setBounds(64, 28, 174, 14);
		contentPane.add(lblNewLabel);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(0, 0, 50, 1));
		spinner.setBounds(10, 25, 44, 20);
		contentPane.add(spinner);
		
		JLabel lblOfertaAElegir = new JLabel("OFERTA A ELEGIR:");
		lblOfertaAElegir.setBounds(10, 56, 154, 14);
		contentPane.add(lblOfertaAElegir);
		
		JRadioButton rdbtnAcumulacionDePuntos = new JRadioButton("ACUMULACION DE PUNTOS");
		rdbtnAcumulacionDePuntos.setBounds(6, 76, 200, 23);
		contentPane.add(rdbtnAcumulacionDePuntos);
		
		JRadioButton rdbtnCarnetJove = new JRadioButton("CARNET JOVE");
		rdbtnCarnetJove.setBounds(6, 102, 200, 23);
		contentPane.add(rdbtnCarnetJove);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("JUBILADOS");
		rdbtnNewRadioButton.setBounds(6, 128, 200, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnPackAmigos = new JRadioButton("PACK AMIGOS");
		rdbtnPackAmigos.setBounds(6, 154, 200, 23);
		contentPane.add(rdbtnPackAmigos);
		
		JRadioButton rdbtnPackFamiliar = new JRadioButton("PACK FAMILIAR");
		rdbtnPackFamiliar.setBounds(6, 180, 200, 23);
		contentPane.add(rdbtnPackFamiliar);
		
		textField = new JTextField();
		textField.setText("0");
		textField.setBounds(10, 213, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblTotalAPagar = new JLabel("TOTAL A PAGAR");
		lblTotalAPagar.setBounds(106, 216, 100, 14);
		contentPane.add(lblTotalAPagar);
		
		JButton btnNewButton = new JButton("ACEPTAR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Logiin logiin = new Logiin();
				logiin.setVisible(true);
			}
		});
		btnNewButton.setBounds(10, 251, 196, 23);
		contentPane.add(btnNewButton);
	}

}
